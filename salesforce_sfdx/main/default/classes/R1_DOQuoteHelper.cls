global without sharing class R1_DOQuoteHelper implements vlocity_ins.VlocityOpenInterface {

    global static Integer lineNum = 100;

    global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outputMap,Map<String,Object> options) {

        List<Object> arguments = (List<Object>) inputMap.get('arguments');
        if(methodName == 'quoteLetterCallout'){
            return quoteLetterCallout(inputMap, outputMap);
        }
        else if(methodName == 'ipInputLog'){
            return ipInputLog(inputMap, outputMap);
        }
        else if(methodName == 'invokeQuoteBind'){
            return invokeQuoteBind(inputMap, outputMap, options);
        } 
        return false;
    }
    
    private Boolean ipInputLog(Map<String,Object> inputMap, Map<String,Object> outputMap) {
        String testLog = JSON.serialize(inputMap);
        String pId = (String)inputMap.get('ContextId');
        Attachment att=new Attachment();
 		att.Body=Blob.valueOf(testLog);
 		att.Name='ipInputLog' + '.txt';
 		att.parentId= pId;
 		insert att;
        return true;
     }
    
    private Boolean quoteLetterCallout(Map<String,Object> inputMap, Map<String,Object> outputMap) {
        System.debug('****authtoken'+ (String)inputMap.get('authToken'));
        System.debug('****apitoken'+ (String)inputMap.get('apiToken'));
        String authToken = (String)inputMap.get('authToken');
        String apiToken = (String)inputMap.get('apiToken');
        httpRequest httpRequest = new httpRequest();
        httpRequest.setHeader('Content-type','application/XML');
        Http httpObject = new Http();
        HttpResponse httpResponse;
        httpRequest.setHeader('Content-type', 'text/xml');
        httpRequest.setHeader('Authorization', authToken);
        httpRequest.setHeader('DigitalApiToken', apiToken);
        httpRequest.setEndpoint('https://nauat.chubbdigital.com/api/v1/documents?type=quoteletter');
        httpRequest.setMethod('GET');
        httpRequest.setTimeout(120000);
        if(!system.test.isRunningTest()) {
            httpResponse = httpObject.send(httpRequest);
            ContentVersion v = new ContentVersion();
            v.versionData = httpResponse.getBodyAsBlob();
        	v.title = 'Quote_Letter.pdf';
        	v.pathOnClient ='Quote_Letter.pdf';
        	v.FirstPublishLocationId = (String)inputMap.get('prequoteId');
        	insert v;
            if(httpResponse.getStatusCode() == 200) {
            system.debug('Response 200 - '+httpResponse.getBody());
            }else{
            system.debug('Response - '+httpResponse.getBody());
            }
        }
       return true;
     }

	private Boolean invokeQuoteBind(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> options) { 
		Map<String, Object> ipOutput = new Map<String, Object> ();
        String procedureName = 'Chubb_DOQuoteBind';
        ipOutput = (Map<String, Object>)vlocity_ins.IntegrationProcedureService.runIntegrationService(procedureName, inputMap, options);
        return true;
    }    
}