/*
* @author Avinash
* @date
* @name: OnbProcessHelper
* @description:
* @Change History
*
*   Tag         Author              Date         Incident No.   Requested-BY      Description
* *****************************************************************************************************************
*   1.0.0       Avinash                                                            Initial Version
*   2.0.0       Ramya Krishna                                                      Added new method "processInsuredAccount"
* *****************************************************************************************************************/
global without sharing class OnbProcessHelper implements vlocity_ins.VlocityOpenInterface {
    private class OnbProcessException extends Exception {}
    
    global Boolean invokeMethod(String methodName,Map<String,Object> inputMap,Map<String,Object> outputMap,Map<String,Object> options) {
        if(methodName == 'convertLead'){
            return convertLead(inputMap, outputMap);
        }else if(methodName == 'updateLead'){
            return updateLead(inputMap, outputMap);
        }else if(methodName == 'dupTinCheck'){
            return dupTinCheck(inputMap, outputMap);
        }else if(methodName == 'accountUpdate'){
            return accountUpdate(inputMap, outputMap);
        }else if(methodName == 'updateContact'){
            return updateContact(inputMap, outputMap);
        }
        return false;
    }
    
    
    private Boolean accountUpdate(Map<String,Object> inputMap, Map<String,Object> outputMap) {
        try { 
            String accId = (String)inputMap.get('convAccountId');
            String UniqueId = (String)inputMap.get('UniqueId');
            String parentAccId;
            String MailingAddressCheck ;
            
            Map<String,Object> agencyInfo = (Map<String,Object>) inputMap.get('AgencyInformation');
            Map<String,Object> agencyDetails = (Map<String,Object>) inputMap.get('AgencyDetails');
            Map<String,Object> certifications = (Map<String,Object>) inputMap.get('Certifications'); 
            Map<String,Object> marketing = (Map<String,Object>) inputMap.get('Marketing'); 
            System.debug('******** AgencyInfo Map ******: '+ agencyInfo);
            System.debug('******** AgencyDetails Map ******: '+ agencyDetails);
            
            String entityJoined = '';
            if(agencyDetails.get('EntityName') != null){
                for(Object entity : (List<Object>) agencyDetails.get('EntityName')) {
                    entityJoined += String.valueOf(entity);
                    entityJoined += ',';
                }
            }
            Map<String,Object> locationAddr;
            Map<String,Object> mailingAddr;
            Map<String,Object> entityValues;
            
            if(agencyInfo.get('AgencyLocationAddress-Block') != null) {
                locationAddr = (Map<String,Object>) agencyInfo.get('AgencyLocationAddress-Block');
                System.debug('Location Map: '+ locationAddr);
            }
            
            if(agencyInfo.get('AgencyMailingAddress-Block') != null){
                mailingAddr = (Map<String,Object>) agencyInfo.get('AgencyMailingAddress-Block');
                System.debug('Mailing Map: '+ mailingAddr);
            }
            
            System.debug('Input Map: '+ JSON.serialize(inputMap));
            Account acc = new Account();
            acc.Id = accId;
            if(inputMap.get('parentAccountId') != null)
            {
                acc.ParentId = (String)inputMap.get('parentAccountId');
            }
            
            acc.Agency_DBA__c = (String)agencyInfo.get('AgencyDBA');
            acc.Agency_IRS_Entity_Type__c = (String)agencyInfo.get('AgencyIRSType');
            acc.Agency_Id__c = UniqueId;
            if(agencyInfo.get('AgencyIRSTypeSubList') != null)
            {
                acc.LLC_Tax_Classification__c = (String)agencyInfo.get('AgencyIRSTypeSubList');
            }
            acc.Name = (String)agencyInfo.get('AgencyLegalName');
            if(agencyInfo.get('Fax') != null)
            {
                acc.Fax = (String)agencyInfo.get('Fax');
            }
            if(agencyInfo.get('Website') != null)
            {
                acc.Website = (String)agencyInfo.get('Website');
            }
            if(agencyInfo.get('TypeofFederalTax') != null)
            {
                acc.TaxIdType__c = (String)agencyInfo.get('TypeofFederalTax');
            }
            if(agencyInfo.get('EINNumber') != null)
            {
                acc.vlocity_ins__TaxID__c  = (String)agencyInfo.get('EINNumber');  
            }
            if(agencyInfo.get('SSNNumber') != null)
            {
                acc.vlocity_ins__TaxID__c  = (String)agencyInfo.get('SSNNumber'); 
            }
            if(agencyInfo.get('NPNNumber') != null)
            {
                acc.agentsync__NPN__c = (String)agencyInfo.get('NPNNumber');                
            }
            if(agencyInfo.get('Phone') != null)
            {
                acc.Phone = (String)agencyInfo.get('Phone');
            }
            if(agencyInfo.get('AgencyLocationAddressLine2') != null)
            {
                acc.Location_Address2__c = (String)agencyInfo.get('AgencyLocationAddressLine2');
            }
             if(agencyInfo.get('MailingAddressLine2') != null)
            {
                acc.Shipping_Address2__c = (String)agencyInfo.get('MailingAddressLine2');
            }
            if(locationAddr != null) {
                acc.Location_City__c = (String)locationAddr.get('LocationCity');
                acc.Location_Country__c = (String)locationAddr.get('LocationCountry');
                acc.Location_Zipcode__c = (String)locationAddr.get('LocationPostalCode');
                acc.Location_State__c = (String)locationAddr.get('LocationState');
                acc.Location_Street__c = (String)locationAddr.get('LocationStreet');
            }
            if(agencyInfo.get('AddressCheck') != null)
            {
                MailingAddressCheck = (String)agencyInfo.get('AddressCheck'); 
            }
            if(MailingAddressCheck == 'Yes' && MailingAddressCheck != null)
            {
                acc.ShippingCity = acc.Location_City__c;
                acc.ShippingCountry = acc.Location_Country__c;
                acc.ShippingPostalCode = acc.Location_Zipcode__c;
                acc.ShippingState = acc.Location_State__c ;
                acc.ShippingStreet = acc.Location_Street__c;
            }
            else if(MailingAddressCheck == 'No' && MailingAddressCheck != null)
            {
                acc.ShippingCity = (String)mailingAddr.get('MailingCity');
                acc.ShippingCountry = (String)mailingAddr.get('MailingCountry');
                acc.ShippingPostalCode = (String)mailingAddr.get('MailingPostalCode');
                acc.ShippingState = (String)mailingAddr.get('MailingState');
                acc.ShippingStreet = (String)mailingAddr.get('MailingStreet');
            }
            
            acc.YearEstablished__c = (String)agencyDetails.get('question1');
            
            if(agencyDetails.get('question2') == 'Yes')
            {
                acc.OwnershipChangedInLast5Yrs__c = true;   
            }
            else if(agencyDetails.get('question2') == 'No')
            {
                acc.OwnershipChangedInLast5Yrs__c = false; 
            }
            
            if(agencyDetails.get('question3') == 'Yes')
            {
                acc.IsAgencyOwnedByOtherEntity__c  = true;
                acc.ReasonForOwnedByOther__c = (String)agencyDetails.get('question3Explanation');
            }
            else if(agencyDetails.get('question3') == 'No')
            {
                acc.IsAgencyOwnedByOtherEntity__c  = false;
            }
            acc.AgencyInsuredAssocCount__c = Decimal.valueOf(String.valueOf(agencyDetails.get('CommunityAssociationCount'))); 
            acc.TotalCommercialPremium__c = Decimal.valueOf(String.valueOf(agencyDetails.get('ComPremiumwithoutComAuto')));
            if(agencyDetails.get('question4') == 'Yes')
            {
                acc.IsWholesalerAggregatorOperator__c = true;  
            }
            else if(agencyDetails.get('question4') == 'No')
            {
                acc.IsWholesalerAggregatorOperator__c = false;    
            }
            if(agencyDetails.get('question5') == 'Yes')
            {
                acc.IsCaptiveAgency__c = true;  
            }
            else if(agencyDetails.get('question5') == 'No')
            {
                acc.IsCaptiveAgency__c = false;    
            }
            acc.CaptiveAgencyType__c = (String)agencyDetails.get('InsuranceCarrierOptions');
            acc.TotalWrittenPremium__c = Decimal.valueOf(String.valueOf(agencyDetails.get('TotalWrittenPremium')));
            acc.TotalStaffInclPartTime__c = Decimal.valueOf(String.valueOf(agencyDetails.get('StaffCount')));
            acc.MergedAcquiredEntitiesList__c = entityJoined;
            //acc.E_O_Expiration_Date__c = Date.valueOf(String.valueOf(certifications.get('E_OExpirationDate')));
            if(certifications.get('E_OExpirationDate') != null){
                acc.EOExpirationDate__c = Date.parse((String) certifications.get('E_OExpirationDate'));
            }
            if(certifications.get('CrimeBondExpirationDate') != null)
            {
               acc.CrimeBondExpirationDate__c = Date.parse((String) certifications.get('CrimeBondExpirationDate')); 
            }
            
            acc.HowHearAboutCAIS__c = (String) marketing.get('MarketingQuestion1');
            if(marketing.get('SearchedonlineSubList') != null)
            {
                acc.FindUsDetails__c = (String) marketing.get('SearchedonlineSubList');
            }
            else if(marketing.get('TradeShowSubList') != null)
            {
                acc.FindUsDetails__c = (String) marketing.get('TradeShowSubList');
            }
            acc.FindUsOtherSources__c = (String) marketing.get('Other');
            acc.AgencyInsuranceProducts__c = (String) marketing.get('MarketingQuestion2');
            update acc;
            return true;
        }
        catch (exception e) {
            throw new OnbProcessException(e.getMessage() + ' ' + e.getStackTraceString());
        }
    }
    
    private Boolean updateContact(Map<String,Object> inputMap, Map<String,Object> outputMap) {
        String contactId = (String)inputMap.get('convContactId');
        String accId = (String)inputMap.get('convAccountId');
        
        if(inputMap.containsKey('ContactInfoAdmin')) 
        {
            Map<String, Object> contactInfoAdmin = (Map<String, Object>) inputMap.get('ContactInfoAdmin');
            if(contactInfoAdmin.get('AdministratorCheck') != null && contactInfoAdmin.get('SignerCheck') != null) {
                String administratorCheck = (String) contactInfoAdmin.get('AdministratorCheck');
                String signerCheck = (String) contactInfoAdmin.get('SignerCheck');
                
                Contact adminContact = new Contact();
                
                Contact signerContact = new Contact();
                signerContact.Contact_Type__c = 'Authorized Signer';
                
                if(administratorCheck == 'Yes') {
                    adminContact.Id = contactId;
                    adminContact.Phone = (contactInfoAdmin.get('UserOfficePhone')!= null)?((String)contactInfoAdmin.get('UserOfficePhone')):null;
                    adminContact.MobilePhone = (contactInfoAdmin.get('UserMobilePhone')!= null)?((String)contactInfoAdmin.get('UserMobilePhone')):null;
                    adminContact.Fax = (contactInfoAdmin.get('UserFAX')!= null)?((String)contactInfoAdmin.get('UserFAX')):null;
                    if(signerCheck == 'Yes')
                    {
                        adminContact.Contact_Type__c = 'Account Administrator;Authorized Signer';
                    }
                    else{
                        adminContact.Contact_Type__c = 'Account Administrator';
                    }
                }
                else if(administratorCheck == 'No') {
                    adminContact.FirstName = (contactInfoAdmin.get('NewFirstName')!= null)?((String)contactInfoAdmin.get('NewFirstName')):null;
                    adminContact.LastName = (contactInfoAdmin.get('NewLastName')!= null)?((String)contactInfoAdmin.get('NewLastName')):null;
                    adminContact.Email = (contactInfoAdmin.get('NewEmail')!= null)?((String)contactInfoAdmin.get('NewEmail')):null;
                    adminContact.Phone = (contactInfoAdmin.get('NewOfficePhone')!= null)?((String)contactInfoAdmin.get('NewOfficePhone')):null;
                    adminContact.MobilePhone = (contactInfoAdmin.get('NewMobilePhone')!= null)?((String)contactInfoAdmin.get('NewMobilePhone')):null;
                    adminContact.Fax = (contactInfoAdmin.get('NewFAX')!= null)?((String)contactInfoAdmin.get('NewFAX')):null;
                    adminContact.AccountId = accId;
                    adminContact.Contact_Type__c = 'Account Administrator';
                }
                
                if(signerCheck == 'Yes' && administratorCheck != 'Yes') {
                    signerContact.Id = contactId;
                    signerContact.Phone = (contactInfoAdmin.get('UserSignerOfficePhone')!= null)?((String)contactInfoAdmin.get('UserSignerOfficePhone')):null;
                    signerContact.MobilePhone = (contactInfoAdmin.get('UserSignerMobilePhone')!= null)?((String)contactInfoAdmin.get('UserSignerMobilePhone')):null;
                    signerContact.Fax = (contactInfoAdmin.get('UserSignerFAX')!= null)?((String)contactInfoAdmin.get('UserSignerFAX')):null;
                } 
                else if(signerCheck == 'No') {
                    signerContact.FirstName = (contactInfoAdmin.get('SignerFirstName')!= null)?((String)contactInfoAdmin.get('SignerFirstName')):null;
                    signerContact.LastName = (contactInfoAdmin.get('SignerLastName')!= null)?((String)contactInfoAdmin.get('SignerLastName')):null;
                    signerContact.Email = (contactInfoAdmin.get('SignerEmail')!= null)?((String)contactInfoAdmin.get('SignerEmail')):null;
                    signerContact.Phone = (contactInfoAdmin.get('SignerOfficePhone')!= null)?((String)contactInfoAdmin.get('SignerOfficePhone')):null;
                    signerContact.MobilePhone = (contactInfoAdmin.get('SignerMobilePhone')!= null)?((String)contactInfoAdmin.get('SignerMobilePhone')):null;
                    signerContact.Fax = (contactInfoAdmin.get('SignerFAX')!= null)?((String)contactInfoAdmin.get('SignerFAX')):null;
                    signerContact.AccountId = accId;
                }
                upsert adminContact;
                
                if(signerCheck == 'No' || (signerCheck == 'Yes' && administratorCheck != 'Yes')) {
                    upsert signerContact;
                }
            }
        }
        return true;
    }
    
    private Boolean updateLead(Map<String,Object> inputMap, Map<String,Object> outputMap) {
        Map<String,Object> aInfo = (Map<String,Object>) inputMap.get('AgencyInformation');
        Lead l = new Lead();
        l.Id = (String)inputMap.get('ContextId');
        l.Phone = (String)aInfo.get('Phone');
        l.Website = (String)aInfo.get('Website');
        update l;
        return true;
    }
    
    private Boolean convertLead(Map<String,Object> inputMap, Map<String,Object> outputMap) {
        updateLead(inputMap, outputMap);
        String leadId = (String)inputMap.get('ContextId');
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(leadId);
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        lc.setDoNotCreateOpportunity(true);
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        Account acc = new Account();
        acc.id = lcr.getAccountId();
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();
        update acc;
        System.assert(lcr.isSuccess());
        outputMap.put('convAccId', lcr.getAccountId());
        outputMap.put('convConId', lcr.getContactId());
        return true;
    }
    private Boolean dupTinCheck(Map<String,Object> inputMap, Map<String,Object> outputMap) {
        list<Account> dupAgent = new list<Account>();
        Map<String,Object> agencyInfo = (Map<String,Object>) inputMap.get('AgencyInformation');
        If((String)agencyInfo.get('TypeofFederalTax') == 'FEIN'){
            String TINNumber = (String)agencyInfo.get('EINNumber');
            TINNumber = TINNumber.left(2) + '-' + TINNumber.right(7);
            dupAgent = [Select id from Account where vlocity_ins__TaxID__c = :TINNumber];
        }else If((String)agencyInfo.get('TypeofFederalTax') == 'SSN'){
            String TINNumber = (String)agencyInfo.get('SSNNumber');
            dupAgent = [Select id from Account where vlocity_ins__TaxID__c = :TINNumber];
        }
        If(dupAgent.size()>0){
            outputMap.put('dupAgent', 'Yes'); 
        }else{
            outputMap.put('dupAgent', 'No'); 
        }
        return true;
    }
    
}