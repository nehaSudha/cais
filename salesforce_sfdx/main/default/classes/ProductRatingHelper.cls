global without sharing class ProductRatingHelper implements vlocity_ins.VlocityOpenInterface {
    global Boolean invokeMethod(String methodName,Map<String,Object> inputMap,Map<String,Object> outputMap,Map<String,Object> options) {
        if(methodName == 'getCoverageLimits'){
            getCoverageLimits(inputMap, outputMap, options);
        }
        return true;
    }
    private void getCoverageLimits(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String, Object> options) {
        
        String state = (String)inputMap.get('wcLocationState');
        String prodCode = (String)inputMap.get('prodCode');
        
        System.debug('State' + state);
       
        List<Coverage_Liability_Limits__mdt> covLimits = new List<Coverage_Liability_Limits__mdt>();
        System.debug('CovLimits ' + covLimits);
        Map<Id, Coverage_Liability_Limits__mdt> covMap = new Map<Id, Coverage_Liability_Limits__mdt>([Select Id, MasterLabel,Value__c,DeveloperName,Included_States__c from Coverage_Liability_Limits__mdt where Product_Code__c = :prodCode Order By DeveloperName asc]);
            for(Id i:covMap.keySet()){            
            if(covMap.get(i).Included_States__c.contains(state)){
               covLimits.add(covMap.get(i)); 
            }
        }         
        List<Map<String,String>> coverageLimits = new List<Map<String, String>>();
		for (Coverage_Liability_Limits__mdt cl :covLimits){ 
			Map<String, String> limitMap = new Map<String, String>();
			limitMap.put('name', (string)cl.get('Value__c'));
            limitMap.put('value', (string)cl.get('MasterLabel'));			
			coverageLimits.add(limitMap);
            System.debug(limitMap);
		}
        System.debug(coverageLimits);
        outputMap.put('options',coverageLimits);        
    }       
}