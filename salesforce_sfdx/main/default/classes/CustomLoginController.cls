public class CustomLoginController {
	
    public String password { get; set; }
    public String username { get; set; }
    
    /*public pageReference doLogin(){
        system.debug('username++++++++++'+username);
        system.debug('password++++++++++'+password);
        system.debug('Logged in');
        return site.login(username,password, '/s');
    }*/
    public PageReference loginUser() {
        username = ApexPages.currentPage().getParameters().get('userName');
        password = ApexPages.currentPage().getParameters().get('password');
        PageReference pg;
        if(String.isBlank(username)) {
            pg = new PageReference('/CustomLoginPageNew');
            pg.getParameters().put('valid', 'false');
            pg.getParameters().put('message', 'Please enter your User Name.');
            return pg;
        }
        if(String.isBlank(password)) {
            pg = new PageReference('/CustomLoginPageNew');
            pg.getParameters().put('valid', 'false');
            pg.getParameters().put('message', 'Please enter your Password.');
            return pg;
        }
        try {
            User usr = [SELECT Id FROM User WHERE Username = :username];
            for(UserLogin ul: [SELECT IsPasswordLocked FROM UserLogin WHERE UserId = :usr.Id]) {
                if(ul.IsPasswordLocked) {
                    pg = new PageReference('/CustomLoginPageNew');
                    pg.getParameters().put('valid', 'false');
                    pg.getParameters().put('message', 'Your account has been locked out due to too many invalid login attempts. Please select the Forgot Password link below to recover your password.');
                    return pg;
                }
            }
			
            pg = Site.login(username, password, '/s');
            if (pg == null) {
                List<ApexPages.Message> msgs = ApexPages.getMessages();
                pg = new PageReference('/CustomLoginPageNew');
                pg.getParameters().put('valid', 'false');
                pg.getParameters().put('message', msgs[0].getSummary());
            }

            return pg;
        } catch(Exception e) {
            pg = new PageReference('/CustomLoginPageNew');
            pg.getParameters().put('valid', 'false');
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            pg.getParameters().put('message', 'Invalid username or password.');
            return pg;
        }
    }
    
}