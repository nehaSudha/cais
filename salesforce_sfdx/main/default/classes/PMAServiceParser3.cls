global class PMAServiceParser3 {
    public qList[] quotes;
        public class qList {
        public String ResponseCode;
        public String ResponseMessage;
        public arrRespData ResponseData;
        }
        public class arrRespData {
            public String PolicyNumber;
            public String RequestId;
        }		
        public static PMAServiceParser3 parse(String json){
            return (PMAServiceParser3) System.JSON.deserialize(json,PMAServiceParser3.class);
        }
        
}