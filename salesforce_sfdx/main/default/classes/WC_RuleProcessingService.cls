/*
 * Required Options: Status, prodFamily/prodCode, quoteId, case1Subject, case1OwnerId, case2OwnerId
 */
global class WC_RuleProcessingService implements vlocity_ins.VlocityOpenInterface {
    public class WC_RuleProcessingServiceException extends Exception {
    }

    public String prodFamily;
    public String prodCode;
    public String Status;
    public String Carrier;
    public String quoteId;
    public String ExtendedQuoteId = '';
    public Boolean ExtendedCaseRequired = false;
    public String caseId;
    public String mailSubject;
    public String mailReferralHeader = '';
    public String mailDeclinedHeader = '';
    public String mailReferralFooter = '';
    public String mailDeclinedFooter = '';
    public String mailFromAddress;
    public Boolean mailRecordLinkRequired = false;

    private Quote quote;
    private User currentUser;

    private List<Case> cases;

    public Map<String, Object> responseMap;

    private final static String EMPTYLINE = '<br/>';
    private final static String SUPPORTTEXT = '<p><span style="color: #003366; font-weight: 400; font-size: 16px;"> Please contact us at (888) 833-4158 with additional questions. </span></p>';
    private final static String STATUS_DECLINED_STATIC_MESSAGE = '<p><span style="color: #003366; font-weight: 400; font-size: 20px;"> This risk does not qualify for our Workers\'Compensation program due to the following response(s): </span></p> ';
    private final static String STATUS_APPROVED_STATIC_MESSAGE = '<p><span style="color: #003366; font-weight: 400; font-size: 20px;"> This risk qualify for our Workers\' Compensation program due to the following response(s): </span></p> ';
    private final static String STATUS_APPROVED_STATIC_MESSAGE_WCIA = '<p><span style="color: #003366; font-weight: 400; font-size: 20px;"> This quote has been referred to an underwriter and is being reviewed for the following response(s): </span></p> '; 
    private final static String STATUS_APPROVED_STATIC_MESSAGE_NO_QUES = '<p><span style="color: #003366; font-weight: 400; font-size: 20px;"> Thank you for your submission. It has been received and is being reviewed by an underwriter. We will be in touch shortly. </span></p> ';
    private final static String STATUS_EMAIL_SENT_SUPPORTTEXT_MESSAGE_CRIME = '<p><span style="color: #003366; font-weight: 400; font-size: 16px;"> We\'ve sent you an email for your records. Please contact us at (888) 833-4158 with additional questions. </span></p>';

    global Boolean invokeMethod(String methodName, Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options) {
        System.debug(JSON.serialize(inputMap));
        System.debug(JSON.serialize(options));

        if (methodName == 'validateRules') {
            validateRules(inputMap, outputMap, options);
        }
        
        return true;
    }

    private void validateRules(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options) {
        try {
            responseMap = new Map<String, Object>();
            cases = new List<Case>();
            Map<String, Map<String, String>> descriptionsMap = new Map<String, Map<String, String>>();

            Status = (String) options.get('Status');
            quoteId = (String) options.get('quoteId');
            if(options.containsKey('ExtendedQuoteId')){
                ExtendedQuoteId = (String) options.get('ExtendedQuoteId');
                if(!String.isBlank(ExtendedQuoteId)){
                    ExtendedCaseRequired = true;//%isCase2Required%
                }
            }
            
            if(options.containsKey('prodFamily')){
                prodFamily = (String) options.get('prodFamily');
            }
            
            if(options.containsKey('prodCode')){
                prodCode = (String) options.get('prodCode');
            }
            
            if(options.containsKey('Carrier')){
                Carrier = (String) options.get('Carrier');
            }
            
            System.debug(Status + ' ' + quoteId + ' ' + prodFamily + ' ' + prodCode);
            
            quote = [SELECT Id, Name, QuoteNumber, AccountId, Account.Name FROM Quote WHERE Id = :quoteId];
            currentUser = [SELECT Id, ContactId, AccountId FROM User WHERE Id = :UserInfo.getUserId()];

            if(prodFamily == 'D&O'){
           		descriptionsMap.putAll(generateDescriptionMapDNO(options));
            }else{
            	descriptionsMap.putAll(generateDescriptionMap(options));
            }

            for(String actionType: descriptionsMap.keySet()) {
                if(actionType.equalsIgnoreCase('OS')) {
                    sendDescriptionToOS(inputMap, outputMap, options, descriptionsMap.get(actionType));
                } else if(actionType.containsIgnoreCase('case1')) {
                    createCaseType1(inputMap, outputMap, options, descriptionsMap.get(actionType), actionType);
                } else if(actionType.containsIgnoreCase('case2')) {
                    //Employee theft only not applicable for California (CA)
                    if(prodFamily == 'Crime' && ExtendedCaseRequired){
                        createCaseType2(inputMap, outputMap, options, descriptionsMap.get(actionType), actionType);
                    }else if(prodFamily != 'Crime'){
                    	createCaseType2(inputMap, outputMap, options, descriptionsMap.get(actionType), actionType);
                    }
                } else if(actionType.containsIgnoreCase('mail')) {
                    mailSubject = (String) options.get('mailSubject');
                    mailReferralHeader = '<p><span style="color: #003366; font-weight: 400; font-size: 20px;"> ' + (String) options.get('mailReferralHeader') + ' </span></p> ';
                    mailReferralFooter = '<p><span style="color: #003366; font-weight: 400; font-size: 20px;"> ' + (String) options.get('mailReferralFooter')+ ' </span></p> ';
                    mailDeclinedHeader = '<p><span style="color: #003366; font-weight: 400; font-size: 20px;"> ' + (String) options.get('mailDeclinedHeader')+ ' </span></p> ';
                    mailDeclinedFooter = '<p><span style="color: #003366; font-weight: 400; font-size: 20px;"> ' + (String) options.get('mailDeclinedFooter')+ ' </span></p> ';
                    if((String) options.get('mailRecordLinkRequired') == 'Yes'){
                        mailRecordLinkRequired = true;
                    }else{
                        mailRecordLinkRequired = false;
                    }
                    sendNotification(inputMap, outputMap, options, descriptionsMap.get(actionType));
                }
            }

            commitCases();

            outputMap.put('Results', responseMap);

        } catch (exception e) {
            throw new WC_RuleProcessingServiceException(e.getMessage() + ' ' + e.getStackTraceString());
        }
    }
    
    private Map<String, Map<String, String>> generateDescriptionMap(Map<String, Object> options) {
        Map<String, Map<String, String>> descriptionsMap = new Map<String, Map<String, String>>();
        
        String selectClause = 'SELECT Product_Family__c, Product_Code__c, Q_Key__c, Q_Ans__c, Q_Label__c, Usage_Type__c, Hide_Value__c, Connector_Value__c, Status__c, Validation__c, OtherElementToDisplay__c ';
        String fromClause = 'FROM Product_Rule_Questions__mdt ';
        String whereClause = 'WHERE Status__c = \'' + Status + '\' AND ';
        if(!String.isBlank(prodFamily) && !String.isBlank(prodCode)){
            whereClause = whereClause + 'Product_Family__c = ' + '\'' + prodFamily + '\'' + ' AND Product_Code__c = ' + '\'' + prodCode + '\'';
        }else if(!String.isBlank(prodFamily)){
            whereClause = whereClause + 'Product_Family__c = ' + '\'' + prodFamily + '\'';
        }else if(!String.isBlank(prodCode)){
            whereClause = whereClause + 'Product_Code__c = '   + '\'' + prodCode   + '\'';
        }

        String query = selectClause + fromClause + whereClause;
        System.debug('selectClause ' + '\n' + selectClause);
        System.debug('fromClause '   + '\n' + fromClause);
        System.debug('whereClause '  + '\n' + whereClause);
        
        for (Product_Rule_Questions__mdt rule : Database.query(query)) {
            if (!String.isBlank(rule.Q_Key__c) && options.containsKey(rule.Q_Key__c)) {
                Boolean ruleValidtionPassed = false;
                String answerEntered = String.valueOf(options.get(rule.Q_Key__c));
				System.debug(rule);
                if ((rule.Validation__c == 'Equal To' && answerEntered == rule.Q_Ans__c)
                        || (rule.Validation__c == 'Does Not Equal' && answerEntered != rule.Q_Ans__c)) {

                    ruleValidtionPassed = true;

                } else if (answerEntered.isNumeric()) {

                    Integer validationAnswer = Integer.valueOf(rule.Q_Ans__c);
                    Integer answerEnteredInteger = Integer.valueOf(answerEntered);

                    if ((rule.Validation__c == 'Less Than' && answerEnteredInteger < validationAnswer)
                            || (rule.Validation__c == 'Greater Than' && answerEnteredInteger > validationAnswer)
                            || (rule.Validation__c == 'Greater Than Or Equal To' && answerEnteredInteger >= validationAnswer)) {

                        ruleValidtionPassed = true;

                    }
                }
				System.debug(ruleValidtionPassed);
                if (ruleValidtionPassed) {
                    if (rule.Usage_Type__c != null) {
                        List<String> usageTypes = rule.Usage_Type__c.split(';');

                        String answer = '';

                        if (!rule.Hide_Value__c) {
                            answer = String.valueOf(options.get(rule.Q_Key__c));
                        }

                        if (!String.isBlank(rule.OtherElementToDisplay__c) && options.containsKey(rule.OtherElementToDisplay__c)) {
                            if (rule.Connector_Value__c == null) {
                                rule.Connector_Value__c = '';
                            }
                            answer += '\r\n' + rule.Connector_Value__c + (String) options.get(rule.OtherElementToDisplay__c);
                        }
                        for (String usageType : usageTypes) {
                            if (!descriptionsMap.containsKey(usageType)) {
                                descriptionsMap.put(usageType, New Map<String, String>());
                            }
                            descriptionsMap.get(usageType).put(rule.Q_Label__c, answer);
                        }
                    }
                }
            }
        }
        System.debug('descriptionsMap: ' + JSON.serialize(descriptionsMap));
        return descriptionsMap;
    }
    
    private Map<String, Map<String, String>> generateDescriptionMapDNO(Map<String, Object> options) {
        Map<String, Map<String, String>> descriptionsMap = new Map<String, Map<String, String>>();
        
        String selectClause = 'SELECT Product_Family__c, Product_Code__c, Carrier__c, Q_Key__c, Q_Ans__c, Q_Label__c, Usage_Type__c, Hide_Value__c, Connector_Value__c, Status__c, Validation__c, OtherElementToDisplay__c ';
        String fromClause = 'FROM Product_Rule_Questions__mdt ';
        String whereClause = 'WHERE Status__c = \'' + Status + '\' AND ';
        if(!String.isBlank(prodFamily) && !String.isBlank(prodCode)){
            whereClause = whereClause + 'Product_Family__c = ' + '\'' + prodFamily + '\'' + ' AND Product_Code__c = ' + '\'' + prodCode + '\'';
        }else if(!String.isBlank(prodFamily)){
            whereClause = whereClause + 'Product_Family__c = ' + '\'' + prodFamily + '\'';
        }else if(!String.isBlank(prodCode)){
            whereClause = whereClause + 'Product_Code__c = '   + '\'' + prodCode   + '\'';
        }
        
        if(!String.isBlank(Carrier)){
            whereClause = whereClause + ' AND Carrier__c = '   + '\'' + Carrier   + '\'';
        }

        String query = selectClause + fromClause + whereClause;
        System.debug('selectClause ' + '\n' + selectClause);
        System.debug('fromClause '   + '\n' + fromClause);
        System.debug('whereClause '  + '\n' + whereClause);
        
        for (Product_Rule_Questions__mdt rule : Database.query(query)) {
            if (!String.isBlank(rule.Q_Key__c) && options.containsKey(rule.Q_Key__c)) {
                Boolean ruleValidtionPassed = false;
                String answerEntered = String.valueOf(options.get(rule.Q_Key__c));
				System.debug(rule);
                if ((rule.Validation__c == 'Equal To' && answerEntered == rule.Q_Ans__c)
                        || (rule.Validation__c == 'Does Not Equal' && answerEntered != rule.Q_Ans__c)) {

                    ruleValidtionPassed = true;

                } else if (answerEntered.isNumeric()) {

                    Integer validationAnswer = Integer.valueOf(rule.Q_Ans__c);
                    Integer answerEnteredInteger = Integer.valueOf(answerEntered);

                    if ((rule.Validation__c == 'Less Than' && answerEnteredInteger < validationAnswer)
                            || (rule.Validation__c == 'Greater Than' && answerEnteredInteger > validationAnswer)
                            || (rule.Validation__c == 'Greater Than Or Equal To' && answerEnteredInteger >= validationAnswer)) {

                        ruleValidtionPassed = true;

                    }
                }
				System.debug(ruleValidtionPassed);
                if (ruleValidtionPassed) {
                    if (rule.Usage_Type__c != null) {
                        List<String> usageTypes = rule.Usage_Type__c.split(';');

                        String answer = '';

                        if (!rule.Hide_Value__c) {
                            answer = String.valueOf(options.get(rule.Q_Key__c));
                        }

                        if (!String.isBlank(rule.OtherElementToDisplay__c) && options.containsKey(rule.OtherElementToDisplay__c)) {
                            if (rule.Connector_Value__c == null) {
                                rule.Connector_Value__c = '';
                            }
                            answer += '\r\n' + rule.Connector_Value__c + (String) options.get(rule.OtherElementToDisplay__c);
                        }
                        for (String usageType : usageTypes) {
                            if (!descriptionsMap.containsKey(usageType)) {
                                descriptionsMap.put(usageType, New Map<String, String>());
                            }
                            descriptionsMap.get(usageType).put(rule.Q_Label__c, answer);
                        }
                    }
                }
            }
        }
        System.debug('descriptionsMap: ' + JSON.serialize(descriptionsMap));
        return descriptionsMap;
    }

    private void sendNotification(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options, Map<String, String> QLabelAnsMap) {

        String caseId = '';
        String mailBodyStsMsg = '';
        String mailBody = '';
        string mailHeader = '';
        string mailFooter = '';
        string recordLink = '<p><span style="color: #003366; font-weight: 400; font-size: 16px;"> <a href=' + '\'' + System.label.Agent_Portal_Link + 'account/' + quote.AccountId + '/' + '\'' + '> View Insured Details at MGAlive.com </a></span></p>';
        string quesAnsPara = buildDescriptionFromMap(QLabelAnsMap, true);
        
        List<String> toAddress = new List<String>();

        toAddress.add(UserInfo.getUserEmail());
        System.debug('User Mail: '+ UserInfo.getUserEmail());

        if(Status == 'Declined'){
        	mailBody = mailBody + quesAnsPara + EMPTYLINE + SUPPORTTEXT + recordLink;
            mailHeader = mailDeclinedHeader;
            mailFooter = mailDeclinedFooter;
        }else if(Status == 'Referral'){
            mailBody = mailBody + quesAnsPara + EMPTYLINE + recordLink;
            mailHeader = mailReferralHeader;
            mailFooter = mailReferralFooter;
        }
        
        mailBody = mailHeader + quesAnsPara + EMPTYLINE + mailFooter;

        if(mailRecordLinkRequired){
            mailBody += recordLink;
        }
        
        System.debug('Mail Body: ' + mailBody);

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(toAddress);
        mail.setSubject(mailSubject);
        mail.setHtmlBody(mailBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
    }

    private void sendDescriptionToOS(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options, Map<String, String> QLabelAnsMap) {
        string contentText = '';
        string quesAnsPara = buildDescriptionFromMap(QLabelAnsMap, true);
        string recordLink = '<p><span style="color: #003366; font-weight: 400; font-size: 16px;"> <a href="' + System.label.Agent_Portal_Link + 'account/' + quote.AccountId + '"> View Insured Details at MGAlive.com </a></span></p>';

        contentText = quesAnsPara + EMPTYLINE; //Header and Footer need to be takencare at individual Omniscript

        responseMap.put('Description', contentText);
        responseMap.put('RecordLink', recordLink);
    }

    //Case with dynamic description.
    private void createCaseType1(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options, Map<String, String> QLabelAnsMap, String actionType) {
        Case cse = new Case();
        cse.Subject = (String) options.get('case1Subject');
        cse.ContactId = currentUser.ContactId;
        cse.AccountId = currentUser.AccountId;
        cse.Description = buildDescriptionFromMap(QLabelAnsMap, false);
        cse.Quote__c = quoteId;
        for(String optionKey: options.keySet()) {
            if(optionKey.startsWithIgnoreCase(actionType) && options.get(optionKey) != null ) {
                String fieldName = optionKey.replace(actionType,'');
                System.debug(fieldName);
                System.debug(actionType);
                String value = String.valueOf(options.get(optionKey));
                cse.put(fieldName, value);
            }
        }
        //cse.Type = 'WC';
        cse.Origin = 'Web';
        cse.Status = 'Open';
        cases.add(cse);
    }

    //Case with dynamic subject
    private void createCaseType2(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options, Map<String, String> QLabelAnsMap, String actionType) {
        Case cse = new Case();
        if(options.containsKey('case2Subject')){
            cse.Subject = (String) options.get('case2Subject');
        }else{
        	cse.Subject = buildDescriptionFromMap(QLabelAnsMap, false);    
        }
        
        cse.ContactId = currentUser.ContactId;
        cse.AccountId = currentUser.AccountId;
        
        if(!String.isBlank(ExtendedQuoteId)){
            cse.Description = buildDescriptionFromMap(QLabelAnsMap, false);
            cse.Quote__c = ExtendedQuoteId;
        }else{
        	cse.Quote__c = quoteId;    
        }
        
        for(String optionKey: options.keySet()) {
            if(optionKey.startsWithIgnoreCase(actionType) && options.get(optionKey) != null ) {
                String fieldName = optionKey.replace(actionType,'');
                String value = String.valueOf(options.get(optionKey));
                cse.put(fieldName, value);
            }
        }
        //cse.Type = 'WC';
        cse.Origin = 'Web';
        cse.Status = 'Open';
        cases.add(cse);
    }

    private void commitCases() {
        List<String> caseIds = new List<String>();

        if (cases.size() > 0) {
            insert cases;
        }
        for (Case aCase : cases) {
            caseIds.add(aCase.Id);
        }
        responseMap.put('Cases', caseIds);
    }

    private String buildDescriptionFromMap(Map<String, String> QLabelAnsMap, Boolean withStyling) {
        String description = withStyling ? EMPTYLINE + '<ul> ' : '';
        for (String ques : QLabelAnsMap.keySet()) {
          description = description + (withStyling ? '<li><p>  <span style="color: #003366; font-weight: 400; font-size: 18px;">' : '') + ques + ' ' + QLabelAnsMap.get(ques) + (withStyling ? '</span></p></li>' : '\n');
            //Commemted description = description + (withStyling ? '<p>  <span style="color: #003366; font-weight: 400; font-size: 18px;">' : '') + ques + ' - ' + QLabelAnsMap.get(ques) + (withStyling ? '</span></p>' : '\n');
        }
        description += (withStyling ?'</ul>':'\n');
        
        /*if (Status == 'Referral' && prodCode == 'WCPay')
        {
            description = '';
        }*/
        
        return description;
    }

}