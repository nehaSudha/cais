/**************************************************************
* Author - Avinash Kudupudi
* Date - 09/29/20 
* Description - Reserful Service to receive status messages from PMA system. 
* Change log -
***************************************************************/
@RestResource(urlMapping='/PMAService/*')
global with sharing class PMAService {
//Http post method to accept the inbound alert JSON from PMI service.    
    @HttpPost
    global static List<Id> doPost() {
        List<Quote> quoteList = new List<Quote>();
        RestRequest req = RestContext.request;
    	Blob body = req.requestBody;
    	String requestString = body.toString();
        PMAServiceParser1 codeList = (PMAServiceParser1)JSON.deserialize(requestString,PMAServiceParser1.class);
        for(PMAServiceParser1.qList cd:codeList.quotes){
            if(cd.ResponseCode <> '306'){
                PMAServiceParser2 p2 = (PMAServiceParser2)JSON.deserialize(requestString,PMAServiceParser2.class);
                Quote q = new Quote();
                q.Id = p2.quotes[0].InternalReferenceId;
                q.API_Response_Code__c = p2.quotes[0].ResponseCode;
                q.API_Response_Message__c = p2.quotes[0].ResponseMessage;
                quoteList.add(q);
            }else{
                PMAServiceParser3 p3 = (PMAServiceParser3)JSON.deserialize(requestString,PMAServiceParser3.class);
                Quote q = new Quote();
                q.Id = p3.quotes[0].ResponseData.RequestId;
                q.API_Response_Code__c = p3.quotes[0].ResponseCode;
                q.API_Response_Message__c = p3.quotes[0].ResponseMessage;
                quoteList.add(q);
            }
        }
		update quoteList;

		Map<Id, Quote> AMap = new Map<Id, Quote>(quoteList);
		return new List<Id>(AMap.keySet());
    }    
}