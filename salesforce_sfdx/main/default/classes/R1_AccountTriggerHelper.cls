public class R1_AccountTriggerHelper {
    public static void agentContractDS(List<Account> agentList){
        R1_AccountHelper accHelper = new R1_AccountHelper();
		Map<String, Object> ipInput = new Map<String, Object> ();
		Map<String, Object> ipOutput = new Map<String, Object> ();
		Map<String, Object> ipOptions = new Map<String, Object> ();
        String methodName = 'agentContractDS';  
        for(Account ag:agentList){
            //if(ag.AgentSync_Agency_Status__c == 'Application Approved'){
                String AgencyId = ag.Id;
            	ipInput.put('Id', AgencyId);
            	Boolean bol = accHelper.invokeMethod(methodName,ipInput,ipOutput,ipOptions);
            //}
            
        }
    }
}