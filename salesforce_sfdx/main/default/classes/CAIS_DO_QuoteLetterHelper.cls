public class CAIS_DO_QuoteLetterHelper {
    public static Boolean sendRequest(String authToken, String apiToken){
        //String xmlBody = 'Request Body';
        httpRequest httpRequest = new httpRequest();
        httpRequest.setHeader('Content-type','application/XML');
        Http httpObject = new Http();
        HttpResponse httpResponse;
        httpRequest.setHeader('Content-type', 'text/xml');
        httpRequest.setHeader('Authorization', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IjVPZjlQNUY5Z0NDd0NtRjJCT0hIeEREUS1EayIsImtpZCI6IjVPZjlQNUY5Z0NDd0NtRjJCT0hIeEREUS1EayJ9.eyJhdWQiOiIwMDAwMDAwMi0wMDAwLTAwMDAtYzAwMC0wMDAwMDAwMDAwMDAiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC9lNTM1ODdmZC02ZjgyLTQ0ODEtYWE2NS1mYTkzMTNjODMxMTIvIiwiaWF0IjoxNjA4NzUyNjkyLCJuYmYiOjE2MDg3NTI2OTIsImV4cCI6MTYwODc1NjU5MiwiYWlvIjoiRTJKZ1lCQzhOZFZCa3NWMzR2bUxvV3hxTmsrMUFBPT0iLCJhcHBpZCI6ImRkMzA1YWNjLTc3MmItNGNmYS1hNWQxLTJkNjMzNjBjN2Q5NCIsImFwcGlkYWNyIjoiMSIsImlkcCI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0L2U1MzU4N2ZkLTZmODItNDQ4MS1hYTY1LWZhOTMxM2M4MzExMi8iLCJvaWQiOiJiOWQzYTE3NC03NTAwLTQxMzQtYjA4Zi02ZTE4YjUxN2RmNzIiLCJyaCI6IjAuQUFBQV9ZYzE1WUp2Z1VTcVpmcVRFOGd4RXN4YU1OMHJkX3BNcGRFdFl6WU1mWlFlQUFBLiIsInN1YiI6ImI5ZDNhMTc0LTc1MDAtNDEzNC1iMDhmLTZlMThiNTE3ZGY3MiIsInRlbmFudF9yZWdpb25fc2NvcGUiOiJOQSIsInRpZCI6ImU1MzU4N2ZkLTZmODItNDQ4MS1hYTY1LWZhOTMxM2M4MzExMiIsInV0aSI6IjcyRk5mTnJTVkVtbHJZT1lzbGRQQUEiLCJ2ZXIiOiIxLjAifQ.Fc0qHy_OT1HEIVFZ435Cudu-Gk2um1ti_YnbhjotE2JM_yVEqHmWG90_C-N8oXmz23m7jD3tUdg8Cf1dgA_AmJyszvv7hgeR_7o_Oq13I261QATmXyBX86yHSMcqriS-k1IsH1XLgeruXgQR3cYBlEWA-bbE1UycA38pbimONY7WKbcv9FnySdF82olAzxvF_n_UCfDA0kewc4xzDmTI1gjn0hJB-GAoOq4pdsSNAtL-ytjFyBalja86tVnYcGkwHmz3BwuGP_ltIqdfA980V7KWaDHvDPvpkOCcM0zmrgJ_-buJgjBTV9R8-naVFKn1BaTs1EOBj-ljKSusgYLNXA');
        httpRequest.setHeader('DigitalApiToken', 'itf3tNW8oPhGshfDN/m+U6nHeTsFFM47nHnJcxO2MODbvNHquQa/WNKgJyuG4zwDSecjZLfTXQA1UWoVUwc5nod9C9ur+wNASDHjKErZppw=');
        httpRequest.setEndpoint('https://nauat.chubbdigital.com/api/v1/documents?type=quoteletter');
        httpRequest.setMethod('GET');
        httpRequest.setTimeout(120000);
        if(!system.test.isRunningTest()) {
            httpResponse = httpObject.send(httpRequest);
            ContentVersion v = new ContentVersion();
            String b = httpResponse.getBody();
            v.versionData = httpResponse.getBodyAsBlob();
        	v.title = '101Quote.pdf';
        	v.pathOnClient ='101Quote.pdf';
        	v.FirstPublishLocationId = '0Q06C000000KiVeSAK'; //simila
        	insert v;
            if(httpResponse.getStatusCode() == 200) {
            system.debug('Response 200 - '+httpResponse.getBody());
            }else{
            system.debug('Response - '+httpResponse.getBody());
            }
        }
        return true;
    }
}