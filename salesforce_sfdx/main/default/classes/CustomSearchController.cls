public class CustomSearchController {
    @AuraEnabled
    public static List<searchResultWrap> searchForIds(String searchText) {
        Set<Id> accountIds = new Set<Id>();
        List<searchResultWrap> searchResults = new List<searchResultWrap>();
        User currentUser = [SELECT Id, AccountId FROM User WHERE Id = :UserInfo.getUserId()];
        accountIds.add(currentUser.AccountId);
        for(Agency_Account_Relationship__c agencyAccountRelationship : [SELECT Id, Insured_Id__c FROM Agency_Account_Relationship__c WHERE Agency_Id__c = :currentUser.AccountId]) {
            accountIds.add(agencyAccountRelationship.Insured_Id__c);
        }

        List<List<SObject>> results = [FIND :searchText IN ALL FIELDS  RETURNING Account(Id, Name, agentsync__ID_FEIN__c), InsurancePolicy(Id, Name, NameInsuredId), Quote(Id, Name, QuoteNumber, AccountId), Case(Id, CaseNumber, Subject, AccountId) LIMIT 1000];

        for (List<SObject> records : results) {
            for (SObject record : records) {
                if(accountIds.contains(record.Id) || (record.getSObjectType() != Schema.Account.getSObjectType() && record.getSObjectType() != Schema.InsurancePolicy.getSObjectType() && record.get('AccountId') != null && accountIds.contains((String) record.get('AccountId'))) || 
                   (record.getSObjectType() == Schema.InsurancePolicy.getSObjectType() && record.get('NameInsuredId') != null && accountIds.contains((String) record.get('NameInsuredId'))) ) {
                    String objectName = record.getSObjectType().getDescribe().getName();
                    searchResultWrap searchResult = new searchResultWrap();
                    searchResult.objectName = objectName;
                    searchResult.record = record;
                    searchResults.add(searchResult);
                }
            }
        }
        return searchResults;
    }

    public class searchResultWrap {
        @AuraEnabled
        public String objectName;
        @AuraEnabled
        public SObject record;
    }
}