@isTest
public class WC_RuleProcessingService_Test {
    
public static testMethod void wcRuleProcessingMessageTest1()
    {
        Account acc = new Account();
        acc.Name = 'xyz';
        acc.Phone = '8899663322';
        acc.Website='www.cais.com';
        acc.Location_City__c='New York';
        insert acc;
        
        Contact cnct = new Contact();
        cnct.LastName = 'pqrs';
        cnct.Email = 'abcd@gmail.com';
        insert cnct;        
        Product2 prd2 = new Product2();
        prd2.Name = 'Name';
        prd2.ProductCode='Crime';
        insert prd2;
        
        Id pricebookId = Test.getStandardPricebookId();       
        Opportunity opp=new Opportunity();
        opp.Name='test one';
        opp.AccountId=acc.Id;
        opp.Description='sample description';
        opp.StageName='Qualifying';
        opp.Amount=23000;
        opp.CloseDate=System.today()+30;
        opp.Pricebook2Id = pricebookId;
        insert opp;
        
        Quote q=new Quote();
        //q.Account__c=acc.Id;
        q.Quote_Status__c='CP';
        q.OpportunityId=opp.Id;
        q.Name='sample quote';
        insert q;
        
        Profile prof = [select id from profile where name LIKE '%System%'];
        
        User user = new User();
        user.firstName = 'test1';
        user.lastName = 'test2';
        user.profileId = prof.id;
        user.username =System.now().millisecond() + 'test2@test.com';
        user.email ='test@test.com';
        user.Alias = 'batman';
        user.EmailEncodingKey='UTF-8';
        user.Firstname='Bruce';
        user.Lastname='Wayne';
        user.LanguageLocaleKey='en_US';
        user.LocaleSidKey='en_US';
        user.TimeZoneSidKey='America/Chicago';
        insert user;
        
        Test.startTest();
        System.runAs(user){
            Map<String, Object> input = new Map<String, Object>();                  
            Map<String, Object> outMap = new Map<String, Object>();
            
            Map<String, Object> options1 = new Map<String, Object>
            {
                'Status' => 'Declined',
                    'prodFamily'=>'Crime',
                    'CompanyOrEmployeesHaveAuthorizationToHandleFunds'=>'Yes',
                    'OtherElementToDisplay__c'=>'IsMaximumDollarAuthorityGTLiabilityText',
                    'quoteId'=>q.id,
                    'Case1_1OwnerId'=>user.Id,
                    'agentLocation'=>'United States'
                    };
                        WC_RuleProcessingService  wcruleproc = new WC_RuleProcessingService();
                        wcruleproc.invokeMethod('validateRules', input, outMap, options1);
        }
        Test.stopTest();
    }
    
    public static testMethod void wcRuleProcessingMessageTest2()
    {
        Account acc = new Account();
        acc.Name = 'xyz';
        acc.Phone = '8899663322';
        acc.Website='www.cais.com';
        acc.Location_City__c='New York';
        insert acc;
        
        Contact cnct = new Contact();
        cnct.LastName = 'pqrs';
        cnct.Email = 'abcd@gmail.com';
        insert cnct;        
        Product2 prd2 = new Product2();
        prd2.Name = 'Name';
        prd2.ProductCode='Crime';
        insert prd2;
        
        Id pricebookId = Test.getStandardPricebookId();       
        Opportunity opp=new Opportunity();
        opp.Name='test one';
        opp.AccountId=acc.Id;
        opp.Description='sample description';
        opp.StageName='Qualifying';
        opp.Amount=23000;
        opp.CloseDate=System.today()+30;
        opp.Pricebook2Id = pricebookId;
        insert opp;
        
        Quote q1=new Quote();
        q1.Quote_Status__c='CP';
        q1.OpportunityId=opp.Id;
        q1.Name='sample quote1';
        insert q1;
        
        Quote q2=new Quote();
        q2.Quote_Status__c='CP';
        q2.OpportunityId=opp.Id;
        q2.Name='sample quote2';
        insert q2;
        
        Profile prof = [select id from profile where name LIKE '%System%'];
        
        User user = new User();
        user.firstName = 'test1';
        user.lastName = 'test2';
        user.profileId = prof.id;
        user.username =System.now().millisecond() + 'test2@test.com';
        user.email ='test@test.com';
        user.Alias = 'batman';
        user.EmailEncodingKey='UTF-8';
        user.Firstname='Bruce';
        user.Lastname='Wayne';
        user.LanguageLocaleKey='en_US';
        user.LocaleSidKey='en_US';
        user.TimeZoneSidKey='America/Chicago';
        insert user;
        
        Test.startTest();
        System.runAs(user){
            Map<String, Object> input = new Map<String, Object>();                  
            Map<String, Object> outMap = new Map<String, Object>();
            
            Map<String, Object> options1 = new Map<String, Object>
            {
                'Status' => 'Declined',
                    'prodFamily'=>'Crime',
                    'prodCode'=>'',
                    'CompanyOrEmployeesHaveAuthorizationToHandleFunds'=>'Yes',
                    'OtherElementToDisplay__c'=>'IsMaximumDollarAuthorityGTLiabilityText',
                    'isCase2Required'=>'Yes',
                    'quoteId'=>q1.id,
                    'ExtendedQuoteId'=>q2.id,
                    'Case1_1OwnerId'=>user.Id,
                    'Case2_1OwnerId'=>user.Id,
                    'agentLocation'=>'United States'
                    };
                        WC_RuleProcessingService  wcruleproc = new WC_RuleProcessingService();
                        wcruleproc.invokeMethod('validateRules', input, outMap, options1);
        }
        Test.stopTest();
    }

}