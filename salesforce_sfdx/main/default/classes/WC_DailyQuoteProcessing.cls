global class WC_DailyQuoteProcessing implements Database.Batchable<SObject>, Database.Stateful, Schedulable{
    
    global WC_DailyQuoteProcessing(){
        
    }
    
    global static void scheduleDaily(){
        //System.schedule('Daily', '0 0 * * * ?', new WC_DailyQuoteProcessing() );
    }
    
    global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new WC_DailyQuoteProcessing(), 200);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = '';
        DateTime currentDateTime = System.now();
        String selectClause = 'SELECT Id, Name, Status, Sub_Status__c, vlocity_ins__EffectiveDate__c, Policy_Term_Effective_Date__c FROM Quote  ';
        String whereClause = ' WHERE Status = \'Approved\' AND vlocity_ins__EffectiveDate__c < :currentDateTime Order by CreatedDate asc ';
        query = selectClause + whereClause;
        System.debug('Query: '+ '\n'+ selectClause + '\n' + whereClause + '\n' + query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Quote> newList){
        
        for(Quote qte: newList){
            qte.Sub_Status__c = 'Expired';
        }
        
        Upsert newList;
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
}