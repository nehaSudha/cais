public without sharing class fileController {
        /*
    * @author          Ramya
    * @version         1
    * @date            29-10-2020
    * @description      string,string
    * @param            
    * @return          
    */
    @AuraEnabled
    public static List<ContentVersion> getFiles(String recordId, string test){
        List<ContentDocumentLink> cdls = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :recordId ];
        List<Id> docIds = new List<Id>();
        for(ContentDocumentLink cdl: cdls){
            docIds.add(cdl.ContentDocumentId);
        }
        //return [SELECT Title, Id, Type__c, FileExtension, LastModifiedDate, FileType, ContentSize, ContentDocumentId  FROM ContentVersion WHERE ContentDocumentId IN: docIds AND origin = 'C' AND isLatest = true ORDER BY LastModifiedDate desc];
        return [SELECT Title, Id, Type__c, FileExtension, LastModifiedDate, FileType, ContentSize, ContentDocumentId, Owner.Name  FROM ContentVersion WHERE ContentDocumentId IN: docIds AND origin = 'C' AND isLatest = true ORDER BY createdDate desc];
    }
    
    /*
    * @author          Ramya
    * @version         1
    * @date            29-10-2020
    * @description      string,string,string
    * @param            
    * @return          
    */
    @AuraEnabled
    public static List<ContentVersion> getSearchResults(String recordId, String searchString, String test) {
        List<ContentDocumentLink> cdls = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :recordId];
        List<Id> docIds = new List<Id>();
        String keyword = '%' + searchString + '%';
        for (ContentDocumentLink cdl : cdls) {
            docIds.add(cdl.ContentDocumentId);
        }
        return [
                SELECT Title, Id, Type__c, FileExtension, LastModifiedDate, FileType, ContentSize, ContentDocumentId,Owner.Name
                FROM ContentVersion
                WHERE ContentDocumentId IN:docIds AND Origin = 'C' AND IsLatest = TRUE AND Title LIKE :keyword
                ORDER BY LastModifiedDate DESC
        ];
    }
    
    /*
    * @author          Ramya
    * @version         1
    * @date            29-10-2020
    * @description      list<string>
    * @param            
    * @return          
    */
    @AuraEnabled
    public static List<ContentVersion> getFilesByIds(list<string> docIds){
        return [SELECT Title, Id, Type__c, FileExtension, LastModifiedDate, FileType, ContentSize, ContentDocumentId  FROM ContentVersion WHERE ContentDocumentId IN: docIds ORDER BY LastModifiedDate desc];
    }

    /*
    * @author          Ramya
    * @version         1
    * @date            29-10-2020
    * @description      
    * @param            
    * @return          
    */
    @AuraEnabled(cacheable=true)
    public static List<string> getFileTypes(String recordId, String viewUpload){
       List<Quote> quoteLst =  [SELECT Id, Line_of_Business__c from Quote WHERE Id=: recordId];
       String quoteLOB = quoteLst[0].Line_of_Business__c;
       List<String> pickListValuesList= new List<String>();
       List<DocumentType__mdt> pickListValues = [SELECT Id, Type__c from DocumentType__mdt WHERE LOB__c=: quoteLOB AND View_Upload__c=: viewUpload];
        // Schema.DescribeFieldResult fieldResult = ContentVersion.Type__c.getDescribe();
        // List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        // for( Schema.PicklistEntry pickListVal : ple){
        //  pickListValuesList.add(pickListVal.getLabel());
        // }     
        // return pickListValuesList;
        List<String> ple = pickListValues[0].Type__c.split(';');
        for( String pl : ple){
            pickListValuesList.add(pl);
        }     
        return pickListValuesList;
    }
    
    /*
    * @author          Ramya
    * @version         1
    * @date            29-10-2020
    * @description      
    * @param            list<string>,string
    * @return          
    */
    @AuraEnabled
    public static List<ContentVersion> updateTypeOnFiles(list<string> docIds,string type){
        List<ContentVersion> cvs = [SELECT Title, Id, Type__c, FileExtension, LastModifiedDate, FileType, ContentSize, ContentDocumentId  FROM ContentVersion WHERE ContentDocumentId IN: docIds];
        List<ContentVersion> cvsToUpdate = new List<ContentVersion>(); 
        for(ContentVersion cv: cvs){
            cv.Type__c = type;
            cvsToUpdate.add(cv);
        }
        update cvsToUpdate;
        return cvsToUpdate;
    }

   /*
    * @author          Ramya
    * @version         1
    * @date            29-10-2020
    * @description      
    * @param            list<string>,string,string,string
    * @return          
    */
    @AuraEnabled
    public static List<ContentVersion> updateTypeDescriptionOnFiles(list<string> docIds,string type,string description,string subType){
        List<ContentVersion> cvs = [SELECT Title, Id, Type__c, FileExtension, LastModifiedDate, FileType, ContentSize, ContentDocumentId  FROM ContentVersion WHERE ContentDocumentId IN: docIds];
        List<ContentVersion> cvsToUpdate = new List<ContentVersion>(); 
        for(ContentVersion cv: cvs){
            cv.Type__c = type;
            cv.description = description;
            // cv.Sub_Type__c = subType;
            cvsToUpdate.add(cv);
        }
        update cvsToUpdate;
        return cvsToUpdate;
    }

}