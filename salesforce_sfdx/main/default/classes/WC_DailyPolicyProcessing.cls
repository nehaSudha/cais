global class WC_DailyPolicyProcessing implements Database.Batchable<SObject>, Database.Stateful, Schedulable{
    
    global WC_DailyPolicyProcessing(){
        
    }
    
    global static void scheduleDaily(){
        //System.schedule('Daily', '0 0 * * * ?', new WC_DailyPolicyProcessing() );
    }
    
    global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new WC_DailyPolicyProcessing(), 200);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = '';
        DateTime currentDateTime = System.now();
        String selectClause = 'SELECT Id, Name, IsRenewedPolicy, ExpirationDate, Status, Substatus, Reason_Code__c FROM InsurancePolicy  ';
        String whereClause = ' WHERE Status = \'In Force\' AND ExpirationDate < :currentDateTime Order by CreatedDate asc ';
        query = selectClause + whereClause;
        System.debug('Query: '+ '\n'+ selectClause + '\n' + whereClause + '\n' + query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<InsurancePolicy> newList){
        Map<Id, InsurancePolicy> RenewalPolicies = new Map<Id, InsurancePolicy>();
        for(InsurancePolicy renewal: [SELECT Id, Name, ParentPolicyId FROM InsurancePolicy WHERE ParentPolicyId IN :newList Order by CreatedDate desc]){
            if(!RenewalPolicies.containsKey(renewal.ParentPolicyId)){
                RenewalPolicies.put(renewal.ParentPolicyId, renewal);
            }
        }
        
        for(InsurancePolicy Policy: newList){
            Policy.Status = 'Expired';
            if(RenewalPolicies.containsKey(Policy.Id)){
                Policy.Substatus = 'Renewed';
            }
        }
        
        Upsert newList;
    }
    
    global void finish(Database.BatchableContext BC){
        
    }

}