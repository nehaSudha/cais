global without sharing class OnbNIPRIntHelper implements vlocity_ins.VlocityOpenInterface {
    
    global static Integer lineNum = 100;

    global Boolean invokeMethod(String methodName,
                                Map<String,Object> inputMap,
                                Map<String,Object> outputMap,
                                Map<String,Object> options) {

        List<Object> arguments = (List<Object>) inputMap.get('arguments');

        system.debug(':::methodName: ' + methodName);
        system.debug(':::inputMap: ' + inputMap);

        if(methodName == 'setUserPass'){
            return setUserPass(inputMap, outputMap);
        }
        else if(methodName == 'editAlertReport'){
            return editAlertReport(inputMap, outputMap);
        }
        else if(methodName == 'editNPNBody2'){
            return editNPNBody2(inputMap, outputMap);
       }

        return false;
    }

       // system.debug(':::options: ' + options);

    private Boolean setUserPass(Map<String,Object> inputMap, Map<String,Object> outputMap) {
         String usr = (String)inputMap.get('USER');
         String pwd = (String)inputMap.get('PWD');

        //String utf8 = EncodingUtil.urlEncode(usr + ':' + pwd, 'UTF-8');
        String utf8 = usr + ':' + pwd;
    	System.debug('utf8 ' + utf8);

        Blob targetBlob = Blob.valueOf(utf8);
        //System.debug('Blob');
        //System.debug(targetBlob.toString());
        String base64 = EncodingUtil.base64Encode(targetBlob);
     	System.debug('base64 ' + base64);
     
        String authValue = 'Basic ' + base64;
     	System.debug(':::authValue: ' + authValue);
        
        outputMap.put('encodeUserPass', authValue);
        
        return true;
    }

 
     private Boolean editAlertReport(Map<String,Object> inputMap, Map<String,Object> outputMap) {
    
          String pdbRept = (String)inputMap.get('pdbReport');
       system.debug('::: pdbRept1: ' + pdbRept.left(25));
          Integer startInt = pdbRept.indexOf('<LicensingReport>');
          pdbRept = pdbRept.substring(startInt);
      system.debug('::: pdbRept2: ' + pdbRept.left(25));
      
          Integer endInt = pdbRept.indexOf('</LicensingReportProcess');
      system.debug(':::end: ' + endInt);
          pdbRept = pdbRept.substring(0, endInt);
      system.debug(':::pdbRept3: '+ pdbRept.right(25));
      
      /* // this calls TD's code directly.  Works but got better results letting a DR do the conversion
          String jsonRept = vlocityins2.XmlToJson.convertXMLToJson(pdbRept);
      system.debug(':::jsonRept: ' + jsonRept.left(50));
      system.debug(':::jsonReptFull: ' + jsonRept);
          pdbRept = jsonRept;
         // outputMap.put('editedRept', jsonRept);  
      */
          outputMap.put('editedRept', pdbRept);
           
          return true;
     }
     
    
    private Boolean editNPNBody2(Map<String,Object> inputMap, Map<String,Object> outputMap) {

        String npnBody = (String)inputMap.get('npnBody');
    	System.debug('****npnBody: ' + npnBody);
        integer first = npnBody.indexOf('<soapenv:Body>');
        integer last = npnBody.indexOf('</soapenv:Envelope>'); 
        npnBody = npnBody.substring(first, last);
        System.debug('****npnBodyTransform: ' + npnBody);
        String hdr = '<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/';
        String hdr2 = '\" xmlns:npn=\"https://pdb.nipr.com/npn-ws/\"><soapenv:Header/>';
        npnBody = hdr + hdr2 +npnBody + '</soapenv:Envelope>';
        System.debug('****npnBodyFinal: ' + npnBody);
        outputMap.put('editedBody', npnBody);    
     
        return true;
     }

}