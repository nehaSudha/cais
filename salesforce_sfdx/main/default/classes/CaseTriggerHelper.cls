public class CaseTriggerHelper {
    public static void createPortalUser(Map<Id, Case> oldMap, Map<Id, Case> newMap){
        set<Id> eligibleList = new set<Id>();
        List<User> usersToInsert = new List<User>();
        
        for(Id i: newMap.keySet()){
            if(newMap.get(i).Type == 'Onboarding' && newMap.get(i).Status =='Approved' && 
               oldMap.get(i).Type == 'Onboarding' && oldMap.get(i).Status !='Approved'){
                   eligibleList.add(newMap.get(i).ContactId);
               }
        }
        
        for(User u: [SELECT Id, Name, ContactId FROM User WHERE ContactId IN :eligibleList]){
            if(eligibleList.contains(u.ContactId)){
                eligibleList.remove(u.ContactId);
            }
        }
        
        Profile partnerProfile = [SELECT Id FROM Profile WHERE Name = 'CAIS Partner Community User'];
        
        if(eligibleList.size() > 0){
            
            for(Contact con: [SELECT Id, FirstName, LastName, Email FROM Contact WHERE Id IN :eligibleList]){
                User u = new User();
                //u.Username = con.Email+'.dev' ;
                u.Username = con.Id+'demotest@cais.com.dev';
                u.FirstName = con.FirstName;
                u.LastName = con.LastName;
                u.Email = con.Email;
                String Nickname = con.FirstName + '.' + con.LastName+ ''+ Integer.valueof((Math.random() * 100))+'';
                if(Nickname.length() < 40){
                    u.CommunityNickname = con.FirstName + '.' + con.LastName+ ''+ Integer.valueof((Math.random() * 100))+'';
                }else{
                    u.CommunityNickname = Nickname.substring(0,39);
                }
                u.Alias = Nickname.substring(0,7);
                u.EmailEncodingKey = 'UTF-8';
                u.TimeZoneSidKey = 'America/Los_Angeles';
                u.LocaleSidKey = 'en_US';
                u.LanguageLocaleKey = 'en_US';
                u.ContactId = con.Id;
                u.ProfileId = partnerProfile.Id;
                usersToInsert.add(u);
            }
            
            if(usersToInsert.size() > 0){
                Database.SaveResult[] srList = Database.insert(usersToInsert, false);
                for (Database.SaveResult sr : srList) {
                    if (sr.isSuccess()) {
                        System.debug('Successfully inserted User. User ID: ' + sr.getId());
                    }
                    else {
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.'); 
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('User fields that affected this error: ' + err.getFields());
                        }
                    }
                }
            }
            
        }
        
    }
    
}