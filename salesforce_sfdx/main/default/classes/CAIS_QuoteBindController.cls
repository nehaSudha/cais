public class CAIS_QuoteBindController {
    @AuraEnabled
    public static void quoteBindRequest(Id recId){
   		R1_DOQuoteHelper qHelper = new R1_DOQuoteHelper();
		Map<String, Object> ipInput = new Map<String, Object> ();
		Map<String, Object> ipOutput = new Map<String, Object> ();
		Map<String, Object> ipOptions = new Map<String, Object> ();
        String methodName = 'invokeQuoteBind'; 
        		String userId = UserInfo.getUserId();
                String quoteId = recId;
            	ipInput.put('userId', userId);
                ipInput.put('quoteId', quoteId);
            	Boolean bol = qHelper.invokeMethod(methodName,ipInput,ipOutput,ipOptions);
        }  
}