global without sharing class CommissionsHelper implements vlocity_ins.VlocityOpenInterface {
    private class OnbProcessException extends Exception {}
    
    global Boolean invokeMethod(String methodName,Map<String,Object> inputMap,Map<String,Object> outputMap,Map<String,Object> options) {
        if(methodName == 'calcCommissions'){
            return calcCommissions(inputMap, outputMap,options);
        }
        return false;
    }  
    private Boolean calcCommissions(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> options) { 
		Map<String, Object> ipOutput = new Map<String, Object> ();
        String procedureName = 'Calculate_AgencyCommissions';
        ipOutput = (Map<String, Object>)vlocity_ins.IntegrationProcedureService.runIntegrationService(procedureName, inputMap, options);
        return true;
    }

}