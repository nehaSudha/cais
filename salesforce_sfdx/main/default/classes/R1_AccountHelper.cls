global without sharing class R1_AccountHelper implements vlocity_ins.VlocityOpenInterface {
    //private class OnbProcessException extends Exception {} 
    global Boolean invokeMethod(String methodName,Map<String,Object> inputMap,Map<String,Object> outputMap,Map<String,Object> options) {
        if(methodName == 'agentContractDS'){
            return agentContractDS(inputMap, outputMap,options);
        }
        return false;
    }  
    private Boolean agentContractDS(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> options) { 
		Map<String, Object> ipOutput = new Map<String, Object> ();
        String procedureName = 'DocuSign_AgencyContract';
        ipOutput = (Map<String, Object>)vlocity_ins.IntegrationProcedureService.runIntegrationService(procedureName, inputMap, options);
        return true;
    }
}