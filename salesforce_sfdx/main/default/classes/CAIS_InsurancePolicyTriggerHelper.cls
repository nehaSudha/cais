public class CAIS_InsurancePolicyTriggerHelper {
	public static void calcCommissions(List<InsurancePolicy> policyList){
        CAIS_CommissionsHelper comHelper = new CAIS_CommissionsHelper();
		Map<String, Object> ipInput = new Map<String, Object> ();
		Map<String, Object> ipOutput = new Map<String, Object> ();
		Map<String, Object> ipOptions = new Map<String, Object> ();
        String methodName = 'calcCommissions';  
        for(InsurancePolicy pol:policyList){
            if(String.isNotBlank(pol.SourceQuoteId )){
                String QuoteId = pol.SourceQuoteId ;
            	String PolicyId = pol.Id;
            	ipInput.put('QuoteId', QuoteId);
            	ipInput.put('PolicyId', PolicyId);
            	Boolean bol = comHelper.invokeMethod(methodName,ipInput,ipOutput,ipOptions);
            }
            
        }
        
    }
}