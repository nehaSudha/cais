trigger CAIS_AccountTrigger on Account (after update) {
    if(System.Label.DeactivateTriggers == 'No'){
        if(Trigger.isUpdate) {
        	if(Trigger.isAfter) {
            	R1_AccountTriggerHelper.agentContractDS(Trigger.new);
        	}
        }
    }    
}