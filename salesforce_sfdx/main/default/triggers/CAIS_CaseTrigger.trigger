trigger CAIS_CaseTrigger on Case (after update) {
    if(System.Label.DeactivateTriggers == 'No'){
		if(Trigger.isUpdate) 
    		{
        	if(Trigger.isAfter) {
            	CaseTriggerHelper.createPortalUser(Trigger.oldMap, Trigger.newMap);
        	}
         }
    }
}