trigger CAIS_InsurancePolicyTrigger on InsurancePolicy (after insert) {
    if(System.Label.DeactivateTriggers == 'No'){
		if(Trigger.isInsert) {
        	if(Trigger.isAfter) {
            	CAIS_InsurancePolicyTriggerHelper.calcCommissions(Trigger.new);
        	}
        }
    }
}