import { LightningElement, track, wire, api } from "lwc";
import getFiles from "@salesforce/apex/fileController.getFiles";
import { NavigationMixin } from "lightning/navigation";

const columns = [
  {
    label: "Title",
    type: "button",
    typeAttributes: {
      title: "Title",
      label: { fieldName: "title" },
      variant: "base",
      alternativeText: "View"
    },
    cellAttributes: { iconName: { fieldName: "icon" }, iconPosition: "left" }
  },
  { label: "Type", fieldName: "type", sortable: true },
  { label: "Size", fieldName: "size", sortable: true },
  { label: "Extension", fieldName: "extension", sortable: true },
  { label: "Last Modified", fieldName: "date", type: "date", sortable: true }
];
export default class CaisFileViewAllComponent extends NavigationMixin(LightningElement) {
  @api recordId;
  @track src;
  @track data = [];
  @track columns = columns;
  @track test = "0";
  parameters = {};
  @track sortBy;
  @track sortDirection;

  @wire(getFiles, { recordId: "$recordId", test: "$test" })
  wiredFiles({ error, data }) {
    if (data) {
      console.log(data);
      let files = [];
      data.forEach(element => {
        let title = element.Title;
        let size = Math.round(element.ContentSize / 1024, 1);
        if (size === 0) {
          size = element.ContentSize + "B";
        } else {
          size = size + "KB";
        }
        let extension = element.FileExtension;
        let date = new Date(element.LastModifiedDate);
        files.push({
          Id: element.Id,
          ContentDocumentId: element.ContentDocumentId,
          title: title,
          size: size,
          extension: extension,
          type: element.Type__c,
          date: date,
          icon: this.icon(element.FileExtension)
        });
      });
      this.data = files;
    } else if (error) {
      console.log("error: ", error);
    }
  }
  connectedCallback() {
    this.parameters = this.getQueryParameters();
    console.log("parameters: ", this.parameters);
    this.recordId = this.parameters.c__recordId;
    let href = window.location.href;
    console.log("href: ", href);
    this.src =
      href.substr(0, href.indexOf("/n/")) + "/r/" + this.recordId + "/view";
    console.log("src: ", this.src);
  }
  icon(fileExtension) {
    let type;
    let imageFileTypes = ["jpg", "tif", "png", "gif"];
    let audioFileTypes = [
      "mp3",
      "wav",
      "ogg",
      "gsm",
      "dct",
      "flac",
      "au",
      "aiff",
      "vox",
      "raw"
    ];
    let videoFileTypes = [
      "avi",
      "wmv",
      "flv",
      "webm",
      "mkv",
      "vob",
      "drc",
      "gifv",
      "mng",
      "mov",
      "qt",
      "yuv",
      "mp4",
      "mpg",
      "3gp"
    ];
    if (imageFileTypes.indexOf(fileExtension) !== -1) {
      type = "image";
    } else if (audioFileTypes.indexOf(fileExtension) !== -1) {
      type = "audio";
    } else if (videoFileTypes.indexOf(fileExtension) !== -1) {
      type = "video";
    } else if (fileExtension === "undefined") {
      type = "unknown";
    } else {
      type = fileExtension;
    }
    return "doctype:" + type;
  }
  getQueryParameters() {
    var params = {};
    var search = location.search.substring(1);

    if (search) {
      params = JSON.parse(
        '{"' + search.replace(/&/g, '","').replace(/=/g, '":"') + '"}',
        (key, value) => {
          return key === "" ? value : decodeURIComponent(value);
        }
      );
    }

    return params;
  }
  handleRowAction(event) {
    const row = event.detail.row;
    let selectedFileId = row.ContentDocumentId;
    event.preventDefault();
    this[NavigationMixin.Navigate]({
      type: "standard__namedPage",
      attributes: {
        pageName: "filePreview"
      },
      state: {
        recordIds: selectedFileId
        //selectedRecordId: selectedFileId
      }
    });
  }
  handleSortdata(event) {
    // field name
    this.sortBy = event.detail.fieldName;

    // sort direction
    this.sortDirection = event.detail.sortDirection;

    // calling sortdata function to sort the data based on direction and selected field
    this.sortData(event.detail.fieldName, event.detail.sortDirection);
  }

  sortData(fieldname, direction) {
    // serialize the data before calling sort function
    let parseData = JSON.parse(JSON.stringify(this.data));

    // Return the value stored in the field
    let keyValue = a => {
      return a[fieldname];
    };

    // cheking reverse direction
    let isReverse = direction === "asc" ? 1 : -1;

    // sorting data
    parseData.sort((x, y) => {
      x = keyValue(x) ? keyValue(x) : ""; // handling null values
      y = keyValue(y) ? keyValue(y) : "";

      // sorting values based on direction
      return isReverse * ((x > y) - (y > x));
    });

    // set the sorted data to data table data
    this.data = parseData;
  }
}