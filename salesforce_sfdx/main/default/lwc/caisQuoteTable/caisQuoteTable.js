import { LightningElement,api } from 'lwc';
import template from "./caisQuoteTable.html";
import { BaseState } from "vlocity_ins/baseState";

export default class CaisQuoteTable extends BaseState(LightningElement) {
    @api tableName;
    @api bundleName;
    @api recordId;
    render() {
        return template;
    }

    connectedCallback(){
        super.connectedCallback();
        this.tableName="Quotes";
        this.bundleName="ReadQuote";
    }
}