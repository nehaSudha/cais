import { LightningElement, track } from "lwc";
import omniscriptEditBlock from "vlocity_ins/omniscriptEditBlock";
import sldsTemplate from "./customLWCeditblock.html"
import ndsTemplate from "./customLWCeditblock_nds.html"
export default class customLWCeditblock extends omniscriptEditBlock{
    // your properties and methods here
    _isFirstIndex = false;
    _hasChildren = false;

    connectedCallback() {
        super.connectedCallback();
    }

    createTableLabelColumns() {
        super.createTableLabelColumns();
        // then loop through _tableLabels and change the cls key
        for (let i = 0; i < this._tableLabels.length; i++) {
            this._tableLabels[i].cls = this._tableLabels[i].cls.replaceAll(this._tableLabels[i].cls.substring(this._tableLabels[i].cls.search(`${this._theme}-size_`), this._tableLabels[i].cls.search("of-12") + 5), '')
            this._tableLabels[i].cls = this._tableLabels[i].cls + " "+ `${this._theme}-size_1-of-`+this._tableLabels.length;
            this._tableLabels[i].cls = this._tableLabels[i].cls.replaceAll('truncate', 'wrap');
            if(this._theme === "nds") {
                this._tableLabels[i].cls = this._tableLabels[i].cls + " " + `${this._theme}`+ "-m-right_x-small ";
                if(this._tableLabels.length >= 9 && this._tableLabels.length <=11) {
                    this._tableLabels[i].cls = this._tableLabels[i].cls + " customWidth ";
                }   
            } else {
                this._tableLabels[i].cls = this._tableLabels[i].cls + " " + `${this._theme}`+ "-m-right_medium ";
                if(this._tableLabels.length >= 9 && this._tableLabels.length <=11) {
                    this._tableLabels[i].cls = this._tableLabels[i].cls + " customWidth ";
                }   
            }
            
        }
    }

    createDisplayColumns() {
        super.createDisplayColumns();
        for (let i = 0; i < this._displayValues.length; i++) {
            
            this._displayValues[i].cls = this._displayValues[i].cls.replaceAll(this._displayValues[i].cls.substring(this._displayValues[i].cls.search(`${this._theme}-size_`), this._displayValues[i].cls.search("of-12") + 5), '')
            this._displayValues[i].cls = this._displayValues[i].cls + " "+ `${this._theme}-size_1-of-`+this._displayValues.length;
            this._displayValues[i].cls = this._displayValues[i].cls.replaceAll('truncate', 'wrap');
            if(this._theme === "nds") {
                //this._displayValues[i].cls = this._displayValues[i].cls + " " + `${this._theme}`+ "-p-horizontal_large ";
                this._displayValues[i].cls = this._displayValues[i].cls + " " + `${this._theme}`+ "-m-right_x-small ";
                if(this._displayValues.length >= 9 && this._displayValues.length <=11) {
                    this._displayValues[i].cls = this._displayValues[i].cls + " customWidth ";
                }
            } else {
                this._displayValues[i].cls = this._displayValues[i].cls + " " + `${this._theme}`+ "-p-horizontal_large ";
                this._displayValues[i].cls = this._displayValues[i].cls + " " + `${this._theme}`+ "-m-right_medium ";
                if(this._displayValues.length >= 9 && this._displayValues.length <=11) {
                    this._displayValues[i].cls = this._displayValues[i].cls + " customWidth ";
                }
            }
        }   
    }
    
    render() {
        this._isFirstIndex = this.jsonDef.index === 0;
        this._hasChildren = this.jsonDef.children.length > 0;
        if (this._theme === "nds") {
          return ndsTemplate;
        }
        return sldsTemplate;
      }
}