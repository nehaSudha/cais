import { LightningElement ,api } from 'lwc';
import template from "./caisGenericTable.html";

export default class CaisGenericTable extends LightningElement {
    @api columns;
    @api records;
    @api tableDetails;
    render() {
        return template;
      }
    renderedCallback(){
        //console.log(JSON.stringify(this.columns),JSON.stringify(this.records), JSON.stringify(this.tableDetails));
    }
}