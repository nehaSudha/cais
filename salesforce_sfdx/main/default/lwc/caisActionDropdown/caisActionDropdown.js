import { LightningElement, track, api } from 'lwc';
import { BaseState } from "vlocity_ins/baseState";
import cardActive from "./caisActionDropdown.html";
import { load as loadNewport } from 'vlocity_ins/newportLoader';

export default class CaisActionDropdown extends BaseState(LightningElement) {
    @track actionSelected;
    render() {
        return cardActive;
    }
    connectedCallback() {
        super.connectedCallback();
        loadNewport(this, false)
          .then(() => {
            //this.isNewport = false;
          })
          .catch(() => {});
      }

    actionSelectedChange(event) {
        let actionIndexSelected = parseInt(event.target.value, 10);
        this.actionSelected = actionIndexSelected !== -1 ? this.actions[actionIndexSelected] : null;
    }

    buttonClick(event){
        event.preventDefault();
    }
}