import { LightningElement } from "lwc";
import omniscriptSelect from "vlocity_ins/omniscriptSelect";
import template from "./customLWCselectblock.html";
export default class customLWCselectblock extends omniscriptSelect{
    
    render() {
      return template;
    }
}