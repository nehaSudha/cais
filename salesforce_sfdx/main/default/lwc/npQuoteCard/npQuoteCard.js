import { LightningElement, api } from "lwc";
import { BaseState } from "vlocity_ins/baseState";
import cardActive from "./npQuoteCard.html";

export default class NpQuoteCard extends BaseState(LightningElement) {

    @api recordId;
    render() {
        return cardActive;
    }

    }