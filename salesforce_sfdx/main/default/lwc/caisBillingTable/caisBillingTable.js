import { LightningElement,api } from 'lwc';
import template from "./caisBillingTable.html";

export default class CaisBillingTable extends LightningElement {
    @api tableName;
    @api bundleName;
    @api recordId;
    @api columns;
    render() {
        return template;
    }

    connectedCallback(){
        this.tableName="Billing";
        this.bundleName="ReadInsuredStatement";
    }
}