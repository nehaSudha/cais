import { LightningElement,api } from 'lwc';
import template from "./caisClaimTable.html";

export default class CaisClaimTable extends LightningElement {
    @api tableName;
    @api bundleName;
    @api recordId;
    @api columns;
    render() {
        return template;
    }

    connectedCallback(){
        this.tableName="Claims";
        this.bundleName="ReadInsuredClaims";
    }
}