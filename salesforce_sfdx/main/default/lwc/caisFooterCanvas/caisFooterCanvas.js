import { LightningElement } from 'lwc';

import template from "./caisFooterCanvas.html";
import { BaseLayout } from "vlocity_ins/baseLayout";
import { load as loadNewport } from 'vlocity_ins/newportLoader';

export default class CaisFooterCanvas extends BaseLayout(LightningElement) {
    constructor() {
        super();
    }

    connectedCallback() {
        super.connectedCallback();
        loadNewport(this, false)
          .then(() => {
            //this.isNewport = false;
          })
          .catch(() => {});
      }
}