import { LightningElement } from 'lwc';
import { BaseState } from "vlocity_ins/baseState";
import cardActive from "./caisContactUsLwc.html";


export default class CaisContactUsLwc extends BaseState(LightningElement) {
    render() {
        return cardActive;
    }
    
    connectedCallback(){
        alert(this.state);
    }
    
}