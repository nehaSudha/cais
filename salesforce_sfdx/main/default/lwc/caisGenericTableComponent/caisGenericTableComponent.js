import { LightningElement, api } from "lwc";
import utility from "vlocity_ins/utility";

export default class CaisGenericTableComponent extends LightningElement{
  @api tableName;
  @api tableData=[];
  @api sortedBy;
  @api bundleName;
  @api recordId;
  @api tableDetails;
  recordSource;
  request;
  @api columns;
  // render() {
  //   return template;
  // }
  sortArrayByAttribute(arr,attr){
    arr.sort((a, b) => (a[attr] > b[attr]) ? 1 : -1);
    return arr;
  }

  connectedCallback() {
    if(this.bundleName){
        this.recordSource = {
          type: "dataraptor",
          value: {
            bundleName: this.bundleName,
            inputMap: {
              Id: this.recordId,
              tableColName:this.tableName,
              tableName:this.tableName,
            }
          }
        };
  
        this.request = JSON.stringify(this.recordSource);
        
        utility.getDataHandler(this.request)
          .then((data) => {
            let dataObj=JSON.parse(data)[0];
            let arr =dataObj["TableColumn"];
            this.columns=this.sortArrayByAttribute(arr,"order");
            this.tableDetails = dataObj.TableDetails;
            this.tableData=dataObj[this.tableName];
          })

          .catch((error) => {
          });
    }
    
  }
}