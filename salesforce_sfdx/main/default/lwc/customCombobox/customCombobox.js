import { LightningElement } from 'lwc';
import combobox from "vlocity_ins/combobox";

export default class CustomCombobox extends combobox {
    connectedCallback() {
        super.connectedCallback();
    }

    renderedCallback() {
        super.renderedCallback();

        if(this.template.querySelector(".nds-dropdown")) {
            this.template.querySelector(".nds-listbox").style.maxHeight = "300px";
            this.template.querySelector(".nds-listbox").style.overflowY = "scroll";
        }
        
    }
    
}