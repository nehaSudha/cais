import { LightningElement, track, api, wire } from "lwc";
import { refreshApex } from "@salesforce/apex";
import getFileTypes from "@salesforce/apex/fileController.getFileTypes";
import getFiles from "@salesforce/apex/fileController.getFiles";
import getSearchResults from "@salesforce/apex/fileController.getSearchResults";
import { NavigationMixin } from "lightning/navigation";
import { deleteRecord } from "lightning/uiRecordApi";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
const actions = [{ label: "Delete", name: "delete" }];
const columns = [
    {
        label: "Title",
        type: "button",
        typeAttributes: {
            title: "Title",
            label: { fieldName: "title" },
            variant: "base",
            alternativeText: "View"
        },
        cellAttributes: { iconName: { fieldName: "icon" }, iconPosition: "left" }
    },
    { label: "Type", fieldName: "type", sortable: true },
    { label: "Size", fieldName: "size", sortable: true },
    { label: "Extension", fieldName: "extension", sortable: true },
    { label: "Uploaded By", fieldName: "owner", sortable: true },
    { label: "Last Modified", fieldName: "date", type: "date", sortable: true },
    {
        type: "action",
        typeAttributes: { rowActions: actions, menuAlignment: "auto" }
    }
];

export default class CaisFileListComponent extends NavigationMixin(LightningElement) {
    @api recordId;
    @track files;
    @track data = [];
    @track columns = columns;
    @track rowOffset = 0;
    @track searchString = "";
    @track showSearch = false;
    @track test = "0";
    categoryFileArray = [];
    @api viewUpload;
    options =[];
    //wiredResult;

    connectedCallback() {
        this.getDataFromServer();
    }

    getDataFromServer() {
        getFiles({ recordId: this.recordId, test: '' })
            .then(data => {
                console.log(data);
                this.data = data;
                this.createFilesArray(data);
            })
            .catch(error => {
                console.log("error" + JSON.stringify(error));
            });
    }

    createFilesArray(data){
      let files = [];
      const months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
    ];
      data.forEach(element => {
        let title = element.Title;
        let size = Math.round(element.ContentSize / 1024, 1);
        if (size === 0) {
            size = element.ContentSize + "B";
        } else {
            size = size + "KB";
        }
        let type = element.Type__c;
        let FileType = element.FileType;
        let extension = element.FileExtension;
        let date = new Date(element.LastModifiedDate);
        let owner = element.Owner.Name;
        date = `${
            months[date.getMonth() - 1]
            } ${date.getDate()}, ${date.getFullYear()}`;
        files.push({
            Id: element.Id,
            ContentDocumentId: element.ContentDocumentId,
            title: title,
            size: size,
            type: type,
            extension: extension,
            date: date,
            FileType: FileType,
            owner:owner,
            icon: this.icon(element.FileExtension)
        });
    });
    this.files = files;
    this.categorizeFilesByType(files);
    }
    /*@wire(getFiles, { recordId: "$recordId", test: "$test" })
    wiredFiles({ error, data }) {
        this.wiredResult = { error, data };
        if (data) {

        } else if (error) {
            console.log("error: ", error);
        }
    }*/

    /*@wire(getSearchResults, {
        recordId: "$recordId",
        searchString: "$searchString",
        test: "$test"
    })
    wiredSearch({ error, data }) {
        if (data) {
            //this.data = data;
            let files = [];
            data.forEach(element => {
                let title = element.Title;
                let size = Math.round(element.ContentSize / 1024, 1);
                if (size === 0) {
                    size = element.ContentSize + "B";
                } else {
                    size = size + "KB";
                }
                let type = element.Type__c;
                let extension = element.FileExtension;
                let date = element.LastModifiedDate;
                files.push({
                    Id: element.Id,
                    ContentDocumentId: element.ContentDocumentId,
                    title: title,
                    size: size,
                    type: type,
                    extension: extension,
                    date: date,
                    icon: this.icon(element.FileExtension)
                });
            });
            this.data = files;
        } else if (error) {
            console.log("error: ", error);
        }
    }*/

    get fileIds() {
        let fileIds = [];
        this.files.forEach(file => {
            fileIds.push(file.ContentDocumentId);
        });
        return fileIds;
    }

    icon(fileExtension) {
        let type;
        let imageFileTypes = ["jpg", "tif", "png", "gif"];
        let audioFileTypes = [
            "mp3",
            "wav",
            "ogg",
            "gsm",
            "dct",
            "flac",
            "au",
            "aiff",
            "vox",
            "raw"
        ];
        let videoFileTypes = [
            "avi",
            "wmv",
            "flv",
            "webm",
            "mkv",
            "vob",
            "drc",
            "gifv",
            "mng",
            "mov",
            "qt",
            "yuv",
            "mp4",
            "mpg",
            "3gp"
        ];
        if (imageFileTypes.indexOf(fileExtension) !== -1) {
            type = "image";
        } else if (audioFileTypes.indexOf(fileExtension) !== -1) {
            type = "audio";
        } else if (videoFileTypes.indexOf(fileExtension) !== -1) {
            type = "video";
        } else if (fileExtension === "undefined") {
            type = "unknown";
        } else {
            type = fileExtension;
        }
        return "doctype:" + type;
    }
    handleChange(event) {
        this.searchString = event.target.value;
        console.log(this.searchString);
        this.handleSearchFiles();
    }

    handleSearchFiles() {
        getSearchResults({ recordId: this.recordId, searchString: this.searchString, test: '' })
            .then(data => {
                console.log(data);
                this.createFilesArray(data);
            })
            .catch(error => {
                console.log("error" + JSON.stringify(error));
            });
    }

    toggleSearchVisibility() {
        this.showSearch = !this.showSearch;
    }

    handleViewAll() {
        
        this[NavigationMixin.Navigate]({
          type: "standard__component",
          attributes: {
              componentName: "c__caisFileViewAllComponent"    
          },    
          state: {
            recordId: this.recordId,    
          }
        });

        // this[NavigationMixin.Navigate]({
        //   type: "standard__recordRelationshipPage",
        //   attributes: {
        //     recordId: this.recordId,
        //     objectApiName: "Quote",
        //     relationshipApiName: "AttachedContentDocuments",
        //     actionName: "view"
        //   }
        // });
        
        // this[NavigationMixin.Navigate]({
        //     type: "standard__navItemPage",
        //     attributes: {
        //         apiName: "View_All_Files"
        //     },
        //     state: {
        //         c__recordId: this.recordId
        //     }
        // });
    }

    navigateToFiles(event) {
        let selectedFileId = event.currentTarget.dataset.docid;
        event.preventDefault();
        this[NavigationMixin.Navigate]({
            type: "standard__namedPage",
            attributes: {
                pageName: "filePreview"
            },
            state: {
                recordIds: selectedFileId
                //selectedRecordId: selectedFileId
            }
        });
    }

    @wire(getFileTypes, { recordId: '$recordId', viewUpload: 'Upload'})
    wiredGetTypes({ error, data }) {
        if (data) {
            let options = [];
            data.forEach(type => {
                options.push({ label: type, value: type });
            });
            this.options = options;
        } else if (error) {
            console.log(error);
        }
    }
    categorizeFilesByType(fileArray){
        // let arrayLRFile = [];
        // let arrayErrorFile = [];
        // let arrayOtherFile = [];
        let arrayFileObj = {};
        fileArray.map(file=>{
        //   if(file.type==='Loss runs'){
        //     arrayLRFile.push(file);
        //   }
        //   if(file.type==='Errors and Omissions'){
        //     arrayErrorFile.push(file);
        //   }
        //   if(file.type==='Other'){
        //     arrayOtherFile.push(file);
        //   }
        if(file.type!==undefined){
        if(arrayFileObj[file.type]!==undefined){
            arrayFileObj[file.type].push(file);
        }
        else{
            arrayFileObj[file.type] = [];
            arrayFileObj[file.type].push(file);
        }
    }
    else{
        if(arrayFileObj['Others']!==undefined){
            arrayFileObj['Others'].push(file);
        }
        else{
            arrayFileObj['Others'] = [];
            arrayFileObj['Others'].push(file);
        }
    }
        })
        
        this.options.map(option=>{
            
                let fileObj = {};
                if(arrayFileObj[option.label]!==undefined){
                    fileObj["Type__c"]=option.label.split(" ").join("");
                    fileObj.files= arrayFileObj[option.label];
                    fileObj.fileLength= arrayFileObj[option.label].length;
                    fileObj.label= option.label;
                    this.categoryFileArray.push(fileObj);
                }
                
            
        });
        if(arrayFileObj["Others"]!==undefined){
            let fileObj = {};
            fileObj["Type__c"]="Others";
            fileObj.files= arrayFileObj["Others"];
            fileObj.fileLength= arrayFileObj["Others"].length;
            fileObj.label= "Others";
        this.categoryFileArray.push(fileObj);
        }
    //     this.categoryFileArray=[{
    //       Type__c:'LossnRun',
    //       files: arrayLRFile,
    //       fileLength: arrayLRFile.length,
    //       label: 'Loss runs'
    //   },
    //       {
    //         Type__c:'ErrornOmission',
    //         files: arrayErrorFile,
    //         fileLength: arrayErrorFile.length,
    //         label: 'Errors and Omissions'
    //       },
    //         {
    //           Type__c:'Other',
    //           files: arrayOtherFile,
    //           fileLength: arrayOtherFile.length,
    //           label: 'Other'
    //     }]
        this.categoryFileArray = JSON.parse(JSON.stringify(this.categoryFileArray));
        console.log(this.categoryFileArray);
    }

    @track openmodel = false;
    openmodal() {
        this.openmodel = true;
    }
    togglePanel(event){
      let id = event.currentTarget.parentElement.getAttribute('data-toggle');
      this.template.querySelector('[id*='+id+"]").style.display = this.template.querySelector('[id*='+id+"]").style.display === 'none' ? '' : 'none';
    }
    closeModal(event) {
        this.openmodel = false;
        // eslint-disable-next-line no-eval
        eval("$A.get('e.force:refreshView').fire();");
        this.test = this.test + "1";
        this.getDataFromServer();
    }

    /*refreshData() {
        this.test = this.test + "1";
        this.getDataFromServer();
    }*/
    refreshData() {
        this.categoryFileArray.length = 0;
        this.getDataFromServer();
    }
    handleRowAction(event) {
        const row = event.detail.row;
        let selectedFileId = row.ContentDocumentId;
        const action = event.detail.action;
        if (action.name === "delete") {
            deleteRecord(selectedFileId).then(() => {
                this.dispatchEvent(
                                    new ShowToastEvent({
                                        title: 'Success',
                                        message: 'Record is successfully deleted',
                                        variant: 'success'
                                    })
                                );
                this.handleSearchFiles();
            });
        } else {
            event.preventDefault();
            this[NavigationMixin.Navigate]({
                type: "standard__namedPage",
                attributes: {
                    pageName: "filePreview"
                },
                state: {
                    recordIds: selectedFileId
                }
            });
        }
    }
}