/* eslint-disable no-console */
import { LightningElement, track, wire, api } from "lwc";
import getFilesByIds from "@salesforce/apex/fileController.getFilesByIds";
import getFileTypes from "@salesforce/apex/fileController.getFileTypes";
import updateTypeOnFiles from "@salesforce/apex/fileController.updateTypeOnFiles";
import { NavigationMixin } from "lightning/navigation";
const columns = [
    {
        label: "View",
        type: "button-icon",
        initialWidth: 75,
        typeAttributes: {
            iconName: "action:preview",
            title: "Preview",
            variant: "border-filled",
            alternativeText: "View"
        }
    },
    { label: "File Name", fieldName: "Title" },
    { label: "Type", fieldName: "Type__c" },
    { label: "Size", fieldName: "ContentSize", type: "number" },
    { label: "Ext", fieldName: "FileExtension" },
    { label: "Last Modified", fieldName: "LastModifiedDate", type: "date" }
];
export default class CaisFileUploadComponent extends NavigationMixin(LightningElement) {
    @api recordId;
    @track options;
    @track selectedOption;
    /*upload related */
    @track showFileUpload = false;
    @track showType = true;
    /*data table related*/
    @track data = [];
    @track columns = columns;
    @track rowOffset = 0;
    @track docIds;
    @api viewUpload;
    @track showTable = false;
    @wire(getFilesByIds, {
        docIds: "$docIds"
    })
    wiredGetFiles({ error, data }) {
        if (data) {
            this.data = data;
            /*updateTypeOnFiles({ docIds: this.docIds, type: this.selectedOption })
                .then(result => {
                    //console.log("result after update: ", result);
                    this.data = result;
                })
                .catch(e => {
                    console.log(e);
                });*/
        } else if (error) {
            console.log(error);
        }
    }

    @wire(getFileTypes, { recordId: '$recordId', viewUpload: 'Upload'})
    wiredGetTypes({ error, data }) {
        if (data) {
            let options = [];
            data.forEach(type => {
                options.push({ label: type, value: type });
            });
            this.options = options;
        } else if (error) {
            console.log(error);
        }
    }
    /*
    get acceptedFormats() {
      return [".pdf", ".txt"];
    }*/
    handleUploadFinished(event) {
        this.showTable = true;
        const uploadedFiles = event.detail.files;
        let docIds = [];
        uploadedFiles.forEach(file => {
            file.Type__c = this.selectedOption;
            docIds.push(file.documentId);
        });
        if (this.docIds !== undefined) {
            this.docIds = docIds.concat(this.docIds);
        } else {
            this.docIds = docIds;
        }
        this.handleFileTypeUpdate();

    }
    handleFileTypeUpdate() {
        updateTypeOnFiles({ docIds: this.docIds, type: this.selectedOption })
            .then(data => {
                const uploadSuccessEvent = new CustomEvent("uploadsuccess", {
                    detail: true
                });
                this.dispatchEvent(uploadSuccessEvent);
            })
            .catch(error => {console.log("error" + JSON.stringify(error));});
        // Dispatches the event.

    }
    handleChange(event) {
        this.selectedOption = event.target.value;
        this.showFileUpload = true;
        this.showType = false;
    }
    resetWindow() {
        this.showFileUpload = false;
        this.showTable = false;
        this.showType = true;
        this.docIds = [];
        this.data = [];
    }
    handleRowAction(event) {
        const row = event.detail.row;
        let selectedFileId = row.ContentDocumentId;
        event.preventDefault();
        this[NavigationMixin.Navigate]({
            type: "standard__namedPage",
            attributes: {
                pageName: "filePreview"
            },
            state: {
                recordIds: selectedFileId
                //selectedRecordId: selectedFileId
            }
        });
    }
}