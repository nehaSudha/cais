import { LightningElement,api } from 'lwc';
import template from "./caisPolicyTable.html";
import { BaseState } from "vlocity_ins/baseState";

export default class CaisPolicyTable extends BaseState(LightningElement) {
    @api tableName;
    @api bundleName;
    @api recordId;
    render() {
        return template;
    }

    connectedCallback(){
        super.connectedCallback();
        this.tableName="Policies";
        this.bundleName="ReadPolicy";
    }
}