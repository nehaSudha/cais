import { LightningElement,api } from 'lwc';
import template from "./caisQuoteBreadcrumb.html";
import { BaseState } from "vlocity_ins/baseState";

export default class CaisQuoteBreadcrumb extends BaseState(LightningElement) {
    @api tableName;
    @api bundleName;
    @api recordId;
    quoteDetailAction;
    render() {
        return template;
    }

    renderedCallback() {
        if(this.recordId){
            this.quoteDetailAction='/s/quote/'+this.recordId+'/detail';
        }
        
      }

}