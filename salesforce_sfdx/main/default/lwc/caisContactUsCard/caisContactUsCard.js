import { LightningElement, api } from 'lwc';
import { BaseState } from "vlocity_ins/baseState";
import cardActive from "./caisContactUsCard.html";

export default class CaisContactUsCard extends BaseState(LightningElement) {
    @api recordId;
    render() {
        return cardActive;
    }
    connectedCallback() {
        super.connectedCallback();
    }


    get firstField() {
        if (this.state && this.state.fields && this.state.fields.length > 0) {
            console.log(JSON.stringify(this.state.fields[0]));
            return this.state.fields[0];
        }
        return [];
    }



}