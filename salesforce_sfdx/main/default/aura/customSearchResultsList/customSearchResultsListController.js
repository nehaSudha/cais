({
    init: function(component, event, helper) {
        var objectsJSON = sessionStorage.getItem('customSearch--recordIds');
        if (!$A.util.isUndefinedOrNull(objectsJSON)) {
            var objects = JSON.parse(objectsJSON);
            component.set('v.objects', objects);
            sessionStorage.removeItem('customSearch--recordIds');
        }
    }
})