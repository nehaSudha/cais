({
    doInit: function (component, event, helper) {
        component.set("v.objects", null);
    },
    onblur: function (component, event, helper) {
        component.set("v.objects", null);
    },
    // This function call when the end User Select any record from the result list.
    handleSearchEvent: function (component, event, helper) {
        const keyPressKey = event.which;
        if(keyPressKey === 13) {
            helper.navigateToCustomSearchPage(component,event);
        } else if(keyPressKey === 27) {
            component.set('v.objects', []);
            component.set('v.recordIds', []);
            component.set('v.showResults', false);
            component.set('v.searchText' , '');
            document.getElementById('searchInput').value = '';
        } else {
            console.log(document.getElementById('searchInput').value);
            var searchKey = document.getElementById('searchInput').value;
            searchKey = searchKey.trim();
            component.set('v.searchText',searchKey);
            helper.handleClick(component, event);
        }
    },

    handleSearchClick: function(component, event, helper) {
        console.log(document.getElementById('searchInput').value);
        var searchKey = document.getElementById('searchInput').value;
        if(searchKey.length > 0){
            helper.navigateToCustomSearchPage(component,event);
        }
    },
    handleOptionSelection: function(component, event, helper) {
        let recordId = event.currentTarget.dataset.id;
        let objectRecs = component.get('v.objects');
        let objectName = '';
        for(var x=0;x<objectRecs.length;x++){
            if(recordId == objectRecs[x].id){
                objectName = objectRecs[x].objectName;
                break;
            }
        }
        if(recordId && objectName){
            component.set('v.objects', []);
            component.set('v.recordIds', []);
            component.set('v.showResults', false);
            var navEvt = $A.get("e.force:navigateToURL");
            navEvt.setParams({ url: '/'+objectName.toLowerCase()+'/'+recordId+'/detail' });
            navEvt.fire();
        }
    }
});