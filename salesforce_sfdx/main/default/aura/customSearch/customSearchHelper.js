({
    handleClick: function (component, event) {
        console.log('Inside handleClick');
        var searchText = component.get('v.searchText');
        if(searchText != undefined  && searchText != '' && searchText.length > 1) {
            console.log(searchText.length);
            var action = component.get('c.searchForIds');
            action.setParams({ searchText: searchText });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    var records = response.getReturnValue();
                    var objectToRecordIds = [];
                    var objects = [];
                    var objectRecords = [];

                    records.forEach(record => {
                        let categoryExists = false;
                        objectRecords.forEach( object => {
                            if(object.name == record.objectName) {
                                categoryExists = true;
                                object.records.push(record);
                            }
                        });
                        if(categoryExists == false) {
                            let object = {
                                name : record.objectName,
                                records : []
                            };
                            object.records.push(record);
                            objectRecords.push(object);
                        }
                    });

                    objectRecords.forEach( object => {
                        var obj = {};
                        obj.name = object.name;
                        obj.records = [];
                        var index = 0;
                        object.records.forEach( record => {
                            let lcaseObjName = object.name;
                            let customRec = {
                                name : record.record.Name?record.record.Name:record.record.CaseNumber+' - '+record.record.Subject,
                                objectName : object.name,
                                lcaseObjectName : lcaseObjName.toLowerCase(),
                                id : record.record.Id
                            };
                            obj.records.push(customRec);
                            if(index < 3) {
                                objects.push(customRec);
                            }
                            index++;
                        });
                        objectToRecordIds.push(obj);
                    });

                    if(component.get('v.searchText').length > 1) {
                        component.set('v.objects', objects);
                        component.set('v.recordIds', objectToRecordIds);
                        if(objects.length > 0) {
                            component.set('v.showResults', true);
                        } else {
                            component.set('v.showResults', false);
                        }
                    } else {
                        component.set('v.objects', []);
                        component.set('v.recordIds', []);
                        component.set('v.showResults', false);
                    }
                } else if (state == 'ERROR') {
                    console.log('Error: ', response.getError());
                }
            });
            $A.enqueueAction(action);
        } else {
            component.set('v.objects', []);
            component.set('v.recordIds', []);
            component.set('v.showResults', false);
        }
    },
    navigateToCustomSearchPage: function (component, event) {
        component.set('v.objects', []);
        component.set('v.showResults', false);
        component.set('v.searchText' , '');
        document.getElementById('searchInput').value = '';
        sessionStorage.setItem('customSearch--recordIds', JSON.stringify(component.get('v.recordIds')));
        var navEvt = $A.get('e.force:navigateToURL');
        navEvt.setParams({ url: '/custom-search-results' });
        navEvt.fire();
    }
})