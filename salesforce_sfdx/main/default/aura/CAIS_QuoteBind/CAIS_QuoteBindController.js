({
	doInit: function(component, event, helper) {
        console.log("In Handler");
        var action = component.get("c.quoteBindRequest");
        console.log("Action -- " + action);

        action.setParams({
            "recId": component.get("v.recordId")
        });
        action.setCallback(this, function(a) {
            console.log("Action Complete");
        });
        $A.enqueueAction(action);
        $A.get("e.force:closeQuickAction").fire();
    }
})