vlocity.cardframework.registerModule.controller('npTableCanvasController', ['$rootScope', '$scope', '$filter', function($rootScope, $scope, $filter) {
    $scope.sessionData = $scope.$parent.$parent.$parent.session.search;
     $scope.sessionTitle = $scope.session.Title;
      $scope.sessionBorder = $scope.session.Border;
      $scope.coverageType = $scope.session.CoverageType;
    var self = this;
    $scope.sortBy = "";
    $scope.sortByFlag = false;
    $scope.currentIndex = 1;
    $scope.totalIndex = 0;
    $scope.setSort = function(value)
    {
        value.name = value.name.replace("['","");
        value.name = value.name.replace("']","");
       $scope.sortByFlag = !$scope.sortByFlag ;
        $scope.sortBy ='-obj.'+ value.name;
    }
    var records = [];
    $scope.parseObjects = function(obj){
        if(obj.hasOwnProperty('childProducts') &&  $scope.coverageType == "CoverageSpec"){
            obj.childProducts.records.forEach(function(childProduct,index){
                childProduct['groupName'] = obj.Name;
                if(childProduct[$rootScope.nsPrefix+'RecordTypeName__c'] ===  $scope.coverageType || childProduct.lineRecordType ===  $scope.coverageType )
                records.push(childProduct);
            });
        }
        else{
            records.push(obj);
        }
        
    }
    $scope.$watch('records', function(oldValue,newValue){
        oldValue.forEach(function(obj){
            $scope.parseObjects(obj);
        });
        $scope.records = records;
        
    });
  
    
    $scope.sum = function(obj)
    {
        $rootScope.sum = 0;
        for(var i = 0; i < records.length; i++) {
           $rootScope.sum += parseFloat(records[i][$rootScope.nsPrefix + "PremiumAmount__c"]["fieldValue"]); 

        }
    }
    function escapeRegExp(str) {
      return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }
    $scope.limitCount = undefined;
    $scope.showPage = function(index,limit)
    {
        //alert($scope.totalIndex + "////////"+ $scope.currentIndex);
        $scope.currentIndex = index;
        $scope.limitCount = limit * $scope.currentIndex;
       
    }
    var getter = $filter('getter');
    var picker = $filter('picker');
    
    function getQueryTextForCard(fields, obj) {
        var text = '';
        fields.forEach(function(field) {
            text += '|' + picker(getter(obj, field), field.type) + '';
        });
        return text;
    }
    
    $scope.$watch('listCardCanvasINSSlds.search.queryableText', function(queryText) {
        if (queryText) {
            var regex = new RegExp(escapeRegExp(queryText), 'i');
            if (!self.cached) {
                self.cached = angular.copy($scope.cards);
            }
            var filtered = [];
            var fullSet = angular.copy(self.cached);
            fullSet.forEach(function(card) {
                var text = getQueryTextForCard(card.fields, card.obj);
                if (regex.test(text)) {
                    filtered.push(card);
                }
            });
            $scope.cards = filtered;
        } else if (self.cached) {
            $scope.cards = self.cached;
            self.cached = null;
        }
    });
    
}]);