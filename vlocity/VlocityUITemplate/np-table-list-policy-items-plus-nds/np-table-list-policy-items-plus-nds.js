vlocity.cardframework.registerModule.controller('npTableListPlusController', ['$rootScope', '$scope', '$filter', function($rootScope, $scope, $filter) {
    $scope.coverageType = $scope.$parent.$parent.$parent.session.CoverageType;
    $scope.checkCoverageSpec = function(obj){
        
        return obj[$rootScope.nsPrefix+'RecordTypeName__c'] === $scope.coverageType || obj.lineRecordType === $scope.coverageType;
    }
    
    $scope.setRowData = function(obj, index){
       
            $scope.row = obj;
            $scope.row.attrs = [];
            console.log($scope.row);
            if(obj.attributeCategories &&  obj.attributeCategories.records){
                for(var i = 0; i < obj.attributeCategories.records.length; i++){
                    if(obj.attributeCategories.records[i].nameResult){
                      for(var j = 0; j < obj.attributeCategories.records[i].nameResult.productAttributes.records.length; j++){
                        $scope.row.attrs.push(obj.attributeCategories.records[i].nameResult.productAttributes.records[j]);
                      }
                    } else if (!obj.attributeCategories.records[i].nameResult && obj.attributeCategories.records[i].productAttributes){
                      for(var j = 0; j < obj.attributeCategories.records[i].productAttributes.records.length; j++){
                        $scope.row.attrs.push(obj.attributeCategories.records[i].productAttributes.records[j]);
                      }
                    }
                }
             }
    
        
    };

   /* $scope.parseObjects = function(obj,index){
        if(obj.hasOwnProperty('childProducts') &&  $scope.coverageType == "CoverageSpec"){
            obj.childProducts.records.forEach(function(childProduct){
                $scope.setRowData(childProduct);
            })
        }
        else{
            $scope.setRowData(obj);
        }
        
    }*/
    
    $scope.getActionSecondaryList = function(actions){
        $scope.actionList = actions.slice(2, actions.length);
    }
    /*$scope.setAttr = function(row, index){
        $rootScope.config.attr =  Object.assign({}, row);
        row.selected = true;
        $rootScope.index = index;
    };*/
    
    
}]);