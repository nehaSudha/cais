declare module "@salesforce/label/c.AgencyBrokerageRecordId_on_Account" {
    var AgencyBrokerageRecordId_on_Account: string;
    export default AgencyBrokerageRecordId_on_Account;
}
declare module "@salesforce/label/c.Agent_Portal_Link" {
    var Agent_Portal_Link: string;
    export default Agent_Portal_Link;
}
declare module "@salesforce/label/c.DeactivateTriggers" {
    var DeactivateTriggers: string;
    export default DeactivateTriggers;
}